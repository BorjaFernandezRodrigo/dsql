import { describe, expect, test } from '@jest/globals';

import { SchemeSqlBuilderService } from '../../../../lib/scheme/domain/services/sql.builder.service';

import { IPrefixPrimitive, ITablePrimitives } from '../../../../lib/public.api';

describe('SchemeSqlBuilderService', () => {
    const table: ITablePrimitives = 'pruebas';
    const prefix: IPrefixPrimitive = 'pr';

    test('Construir una sql valida con todos los campos', () => {
        expect(SchemeSqlBuilderService.build(table, prefix)).toEqual(`pr_pruebas`);
    });

    test('Construir una sql valida solo con campos obligatorios', () => {
        expect(SchemeSqlBuilderService.build(table)).toEqual(`pruebas`);
    });
});
