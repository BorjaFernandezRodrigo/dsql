import { describe, expect, jest, test } from '@jest/globals';

import { SchemeAgregator } from '../../../lib/scheme/domain/scheme.agregator';
import { PipelineError } from '../../../lib/shared/domain/error/pipeline.error';
import { QueryFormatError } from '../../../lib/shared/domain/error/query.format.error';

import { IPipeHandler, IPrefixPrimitive, ITablePrimitives } from '../../../lib/public.api';

describe('DeleteAgregator', () => {
    const table: ITablePrimitives = 'pruebas';
    const prefix: IPrefixPrimitive = 'pr';

    test('crear una instancia con valores validos y verificar que se instancian los value objects', () => {
        const sql = new SchemeAgregator(table);
        expect(sql).toBeInstanceOf(SchemeAgregator);
    });

    test('Crear una instancia DeleteAgregator con valor invalido en table', () => {
        const tableAux: ITablePrimitives = '"1=1"';
        expect(() => new SchemeAgregator(tableAux, prefix)).toThrow(QueryFormatError);
    });

    test('Crear una instancia DeleteAgregator con valor invalido en prefix', () => {
        const prefixAux: IPrefixPrimitive = '"1=1"';
        expect(() => new SchemeAgregator(table, prefixAux)).toThrow(QueryFormatError);
    });

    test('Crear una instancia Insert y validar toprimitives', () => {
        const toPrimitives = new SchemeAgregator(table, prefix).toPrimitives();
        expect(toPrimitives).toEqual({ table, prefix });
    });

    test('Ejecutar pipeline correctamente', () => {
        const agregator = new SchemeAgregator(table, prefix);
        const resultPipeline = agregator.runPipeline({
            data: [],
            affectedRows: 0,
        });
        expect(resultPipeline).toEqual([]);
    });

    test('Error en pipeline se espera error PipelineError', () => {
        const pipeHandler: IPipeHandler = {
            runPipe: jest.fn(() => {
                throw Error();
            }),
            canChannel: jest.fn(() => true),
        };
        const agregator = new SchemeAgregator(table, prefix);
        expect(() =>
            agregator.runPipeline(
                {
                    data: [],
                    affectedRows: 0,
                },
                [pipeHandler]
            )
        ).toThrow(PipelineError);
    });
});
