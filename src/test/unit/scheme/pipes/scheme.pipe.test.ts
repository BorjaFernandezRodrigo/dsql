import { describe, expect, test } from '@jest/globals';

import { SchemePipe } from '../../../../lib/scheme/domain/pipes/scheme.pipe';

describe('SchemePipe', () => {
    test('Crear una instancia SchemePipe', () => {
        const instance = new SchemePipe();
        expect(instance).toBeInstanceOf(SchemePipe);
    });

    test('Al correr el pipeline se devuelve el resultado esperado', () => {
        const instance = new SchemePipe();
        expect(
            instance.runPipe({
                affectedRows: 0,
                data: [
                    {
                        col1: 'test 1',
                        col2: 'test 2',
                    },
                ],
            })
        ).toEqual([
            {
                col1: 'test 1',
                col2: 'test 2',
            },
        ]);
    });
});
