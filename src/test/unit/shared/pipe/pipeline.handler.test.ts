import { describe, expect, test } from '@jest/globals';

import { PipeLineHandler } from '../../../../lib/shared/domain/pipe/pipeline.handler';
import { IOperation } from '../../../../lib/shared/domain/pipe/types/operations.type';

import { IPipeHandler } from '../../../../lib/public.api';

export class TestPipe implements IPipeHandler {
    public runPipe() {
        return {
            col1: 'test 1',
            col2: 'test 2',
        };
    }

    public canChannel(operation: IOperation): boolean {
        return operation === 'select';
    }
}

describe('PipeLineHandler', () => {
    test('Crear una instancia PipeLineHandler', () => {
        const instance = new PipeLineHandler('select');
        expect(instance).toBeInstanceOf(PipeLineHandler);
    });

    test('Al correr el pipeline se devuelve el resultado esperado', () => {
        const instance = new PipeLineHandler('select');
        instance.addPipes([new TestPipe()]);
        expect(instance.runPipeLine([], { columns: '*' })).toEqual({
            col1: 'test 1',
            col2: 'test 2',
        });
    });
});
