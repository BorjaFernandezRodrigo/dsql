import { describe, expect, test } from '@jest/globals';

import { ConditionsService } from '../../../../lib/shared/domain/sql/services/conditions.service';

import { IConditionPrimitives } from '../../../../lib/public.api';

describe('ConditionsService', () => {
    test('Contruir el string sql', () => {
        const where: IConditionPrimitives[] = [
            {
                column: 'id',
                value: 1,
                condition: '=',
            },

            {
                column: 'name',
                value: 'PEPE',
                condition: '=',
            },
        ];
        const result = ConditionsService.Build('WHERE', where);
        const whereSql = ` WHERE id = '1' AND name = 'PEPE'`;
        expect(typeof result).toBe('string');
        expect(result).toBe(whereSql);
    });

    test('Contruir el string sql in', () => {
        const where: IConditionPrimitives[] = [
            {
                column: 'id',
                value: [1, 2],
                condition: 'in',
            },
            {
                column: 'id',
                value: [1, 2],
                condition: 'notIn',
            },
        ];
        const result = ConditionsService.Build('WHERE', where);
        const whereSql = ` WHERE id IN ('1','2') AND id NOT IN ('1','2')`;
        expect(typeof result).toBe('string');
        expect(result).toBe(whereSql);
    });

    test('Contruir el string sql between', () => {
        const where: IConditionPrimitives[] = [
            {
                column: 'id',
                value: [1, 2],
                condition: 'between',
            },
            {
                column: 'id',
                value: [1, 2],
                condition: 'between',
            },
        ];
        const result = ConditionsService.Build('WHERE', where);
        const whereSql = ` WHERE id BETWEEN '1' AND '2' AND id BETWEEN '1' AND '2'`;
        expect(typeof result).toBe('string');
        expect(result).toBe(whereSql);
    });
});
