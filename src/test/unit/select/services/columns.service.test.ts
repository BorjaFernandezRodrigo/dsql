import { describe, expect, test } from '@jest/globals';

import { ColumnsSelectService } from '../../../../lib/select/domain/services/columns.service';

import { IColumnsPrimitives } from '../../../../lib/public.api';

describe('ColumnsSelectService', () => {
    const columns: IColumnsPrimitives = {
        id: { type: 'int', alias: 'ID' },
        name: { type: 'string' },
    };

    test('Contruir el string sql', () => {
        const result = ColumnsSelectService.Build(columns);
        const columnsSql = `SELECT id AS 'ID', name`;
        expect(typeof result).toBe('string');
        expect(result).toBe(columnsSql);
    });

    test('Contruir el string sql', () => {
        const result = ColumnsSelectService.Build('*');
        const columnsSql = `SELECT *`;
        expect(typeof result).toBe('string');
        expect(result).toBe(columnsSql);
    });
});
