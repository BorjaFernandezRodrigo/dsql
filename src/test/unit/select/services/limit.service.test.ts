import { describe, expect, test } from '@jest/globals';

import { LimitSelectService } from '../../../../lib/select/domain/services/limit.service';

import { ILimitPrimitives } from '../../../../lib/public.api';

describe('LimitSelectService', () => {
    test('Contruir el string sql', () => {
        const limit: ILimitPrimitives = 10;
        const result = LimitSelectService.Build(limit);
        const limitSql = ` LIMIT 10`;
        expect(typeof result).toBe('string');
        expect(result).toBe(limitSql);
    });
});
