import { describe, expect, test } from '@jest/globals';

import { SelectAgregator } from '../../../../lib/select/domain/select.agregator';
import { SelectSqlBuilderService } from '../../../../lib/select/domain/services/sql.builder.service';

import {
    IColumnsPrimitives,
    IConditionPrimitives,
    IFromPrimitives,
    IGroupByPrimitives,
    IJoinPrimitives,
    ILimitPrimitives,
    IOrderByPrimitives,
} from '../../../../lib/public.api';

describe('SelectSqlBuilderService', () => {
    const columns: IColumnsPrimitives = {
        id: { type: 'int', alias: 'ID' },
        name: { type: 'string' },
    };
    const from: IFromPrimitives = {
        table1: { alias: 't' },
    };
    const joins: IJoinPrimitives[] = [
        {
            clausule: '<',
            from: {
                table2: { alias: 't2' },
            },
            columnsUnion: {
                id: 't.id',
                name: 't.name',
            },
        },
    ];
    const where: IConditionPrimitives[] = [
        {
            column: 'id',
            value: 1,
            condition: '=',
        },
    ];
    const groupBy: IGroupByPrimitives = {
        by: ['id'],
        having: [
            {
                column: 'name',
                value: 'John',
                condition: '=',
            },
        ],
    };
    const limit: ILimitPrimitives = 10;
    const orderBy: IOrderByPrimitives = {
        orde: 'ASC',
        columns: ['name'],
    };

    test('Construir una sql valida con todos los campos', () => {
        const select = new SelectAgregator(columns, from, joins, where, groupBy, limit, orderBy);
        expect(SelectSqlBuilderService.build(select, 'pr')).toEqual(
            // eslint-disable-next-line max-len
            `SELECT id AS 'ID', name FROM pr_table1 AS t LEFT JOIN pr_table2 AS t2 ON id = t.id AND name = t.name WHERE id = '1' GROUP BY id HAVING name = 'John' LIMIT 10 ORDER BY name ASC;`
        );
    });

    test('Construir una sql valida solo con campos obligatorios', () => {
        const select = new SelectAgregator(columns);
        expect(SelectSqlBuilderService.build(select, 'pr')).toEqual(`SELECT id AS 'ID', name;`);
    });
});
