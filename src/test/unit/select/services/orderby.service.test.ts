import { describe, expect, test } from '@jest/globals';

import { OrderBySelectService } from '../../../../lib/select/domain/services/orderby.service';

import { IOrderByPrimitives } from '../../../../lib/public.api';

describe('OrderBySelectService', () => {
    test('Contruir el string sql', () => {
        const orderBy: IOrderByPrimitives = {
            orde: 'ASC',
            columns: ['name', 'id'],
        };
        const result = OrderBySelectService.Build(orderBy);
        const orderBySql = ` ORDER BY name, id ASC`;
        expect(typeof result).toBe('string');
        expect(result).toBe(orderBySql);
    });
});
