import { describe, expect, test } from '@jest/globals';

import { GroupBySelectService } from '../../../../lib/select/domain/services/groupby.service';

import { IGroupByPrimitives } from '../../../../lib/public.api';

describe('GroupBySelectService', () => {
    test('Contruir el string sql', () => {
        const groupBy: IGroupByPrimitives = {
            by: ['id', 'name'],
            having: [
                {
                    column: 'name',
                    value: 'John',
                    condition: '=',
                },
                {
                    column: 'id',
                    value: 1,
                    condition: '=',
                },
            ],
        };

        let result = GroupBySelectService.Build(groupBy);
        let groupbySql = ` GROUP BY id, name HAVING name = 'John' AND id = '1'`;
        expect(typeof result).toBe('string');
        expect(result).toBe(groupbySql);

        groupBy.having = [];
        result = GroupBySelectService.Build(groupBy);
        groupbySql = ' GROUP BY id, name';
        expect(typeof result).toBe('string');
        expect(result).toBe(groupbySql);
    });
});
