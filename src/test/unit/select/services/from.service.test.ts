import { describe, expect, test } from '@jest/globals';

import { FromSelectService } from '../../../../lib/select/domain/services/from.service';

import { IFromPrimitives } from '../../../../lib/public.api';

describe('FromSelectService', () => {
    const from: IFromPrimitives = {
        table1: { alias: 't' },
    };

    test('Contruir el string sql', () => {
        const result = FromSelectService.Build(from);
        const fromSql = ` FROM table1 AS t`;
        expect(typeof result).toBe('string');
        expect(result).toBe(fromSql);
    });

    test('Contruir el string sql con prefijo', () => {
        const result = FromSelectService.Build(from, 'prefijo');
        const fromSql = ` FROM prefijo_table1 AS t`;
        expect(typeof result).toBe('string');
        expect(result).toBe(fromSql);
    });
});
