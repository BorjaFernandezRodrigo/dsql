import { describe, expect, test } from '@jest/globals';

import { JoinSelectService } from '../../../../lib/select/domain/services/joins.service';

import { IJoinPrimitives } from '../../../../lib/public.api';

describe('JoinSelectService', () => {
    test('Contruir el string sql', () => {
        const joins: IJoinPrimitives[] = [
            {
                clausule: '<',
                from: {
                    table2: { alias: 't2' },
                },
                columnsUnion: {
                    id: 't.id',
                    name: 't.name',
                },
            },
        ];
        const result = JoinSelectService.Build(joins);
        const groupbySql = ` LEFT JOIN table2 AS t2 ON id = t.id AND name = t.name`;
        expect(typeof result).toBe('string');
        expect(result).toBe(groupbySql);
    });
});
