import { describe, expect, test } from '@jest/globals';
import moment from 'moment';

import { FormatSelectPipe } from '../../../../lib/select/domain/pipes/format.pipe';

describe('FormatSelectPipe', () => {
    test('Crear una instancia FormatSelectPipe', () => {
        const instance = new FormatSelectPipe();
        expect(instance).toBeInstanceOf(FormatSelectPipe);
    });

    test('Al correr el pipeline se devuelve el resultado esperado.', () => {
        const instance = new FormatSelectPipe();
        expect(
            instance.runPipe(
                [
                    {
                        col1: 'test 1',
                        col2: 'test 2',
                    },
                ],
                { columns: '*' }
            )
        ).toEqual([
            {
                col1: 'test 1',
                col2: 'test 2',
            },
        ]);
    });

    test('Al que los formatos sean los correctos.', () => {
        const instance = new FormatSelectPipe();
        expect(
            instance.runPipe(
                [
                    {
                        Col1: 'test 1',
                        Col2: '1',
                        Col3: '{ "key": "value" }',
                        Col4: 'true',
                        Col5: '2021-11-02',
                        Col6: '2021-11-02',
                        Col7: '[1, 2]',
                        Col8: '',
                    },
                ],
                {
                    columns: {
                        value1: {
                            alias: 'Col1',
                            type: 'string',
                        },
                        value2: {
                            alias: 'Col2',
                            type: 'int',
                        },
                        value3: {
                            alias: 'Col3',
                            type: 'json',
                        },
                        value4: {
                            alias: 'Col4',
                            type: 'boolean',
                        },
                        value5: {
                            alias: 'Col5',
                            type: 'date',
                        },
                        value6: {
                            alias: 'Col6',
                            type: 'moment',
                        },
                        value7: {
                            alias: 'Col7',
                            type: 'array',
                        },
                        value8: {
                            alias: 'Col8',
                        },
                    },
                }
            )
        ).toEqual([
            {
                Col1: 'test 1',
                Col2: 1,
                Col3: { key: 'value' },
                Col4: true,
                Col5: new Date('2021-11-02'),
                Col6: moment('2021-11-02'),
                Col7: [1, 2],
                Col8: '',
            },
        ]);
    });
});
