import { describe, expect, test } from '@jest/globals';

import { SelectPipe } from '../../../../lib/select/domain/pipes/select.pipe';

describe('SelectPipe', () => {
    test('Crear una instancia SelectPipe', () => {
        const instance = new SelectPipe();
        expect(instance).toBeInstanceOf(SelectPipe);
    });

    test('Al correr el pipeline se devuelve el resultado esperado', () => {
        const instance = new SelectPipe();
        expect(
            instance.runPipe({
                affectedRows: 0,
                data: [
                    {
                        col1: 'test 1',
                        col2: 'test 2',
                    },
                ],
            })
        ).toEqual([
            {
                col1: 'test 1',
                col2: 'test 2',
            },
        ]);
    });
});
