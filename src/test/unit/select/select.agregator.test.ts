import { describe, expect, jest, test } from '@jest/globals';

import { SelectAgregator } from '../../../lib/select/domain/select.agregator';
import { PipelineError } from '../../../lib/shared/domain/error/pipeline.error';
import { QueryFormatError } from '../../../lib/shared/domain/error/query.format.error';

import {
    IColumnsPrimitives,
    IConditionPrimitives,
    IFromPrimitives,
    IGroupByPrimitives,
    IJoinPrimitives,
    ILimitPrimitives,
    IOrderByPrimitives,
    IPipeHandler,
} from '../../../lib/public.api';

describe('Select', () => {
    const columns: IColumnsPrimitives = {
        id: { type: 'int', alias: 'ID' },
        name: { type: 'string' },
    };
    const from: IFromPrimitives = {
        table1: { alias: 't' },
    };
    const joins: IJoinPrimitives[] = [
        {
            clausule: '<',
            from: {
                table2: { alias: 't2' },
            },
            columnsUnion: {
                id: 't.id',
                name: 't.name',
            },
        },
    ];
    const where: IConditionPrimitives[] = [
        {
            column: 'id',
            value: 1,
            condition: '=',
        },
    ];
    const groupBy: IGroupByPrimitives = {
        by: ['id'],
        having: [
            {
                column: 'name',
                value: 'John',
                condition: '=',
            },
        ],
    };
    const limit: ILimitPrimitives = 10;
    const orderBy: IOrderByPrimitives = {
        orde: 'ASC',
        columns: ['name'],
    };

    test('crear una instancia con valores validos y verificar que se instancian los value objects', () => {
        const select = new SelectAgregator(columns, from, joins, where, groupBy, limit, orderBy);
        expect(select).toBeInstanceOf(SelectAgregator);
    });

    test('Crear una instancia Select con valor invalido en select', () => {
        const columnsAux: IColumnsPrimitives = {
            SELECT: {},
        };
        expect(() => new SelectAgregator(columnsAux)).toThrow(QueryFormatError);
    });

    test('Crear una instancia Select con valor invalido en from', () => {
        const fromAux: IFromPrimitives = {
            from: {},
        };
        expect(() => new SelectAgregator(columns, fromAux)).toThrow(QueryFormatError);
    });

    test('Crear una instancia Select con valor invalido en join', () => {
        let joinsAux: IJoinPrimitives[] = [
            {
                clausule: '<',
                from: {
                    table: { alias: 't2' },
                },
                columnsUnion: {
                    id: 't.id',
                    name: 't.name',
                },
            },
        ];
        expect(() => new SelectAgregator(columns, from, joinsAux)).toThrow(QueryFormatError);
        joinsAux = [
            {
                clausule: '<',
                from: {
                    table2: { alias: 't2' },
                },
                columnsUnion: {
                    DROP: 't.id',
                    name: 't.name',
                },
            },
        ];
        expect(() => new SelectAgregator(columns, from, joinsAux)).toThrow(QueryFormatError);
        joinsAux = [
            {
                clausule: '<',
                from: {
                    table2: { alias: 't2' },
                },
                columnsUnion: {
                    name: 't.DROP',
                },
            },
        ];
        expect(() => new SelectAgregator(columns, from, joinsAux)).toThrow(QueryFormatError);
        joinsAux = [
            {
                clausule: '<',
                from: {
                    table2: { alias: 'DROP' },
                },
                columnsUnion: {
                    name: 't.name',
                },
            },
        ];
        expect(() => new SelectAgregator(columns, from, joinsAux)).toThrow(QueryFormatError);
    });

    test('Crear una instancia Select con valor invalido en where', () => {
        let whereAux: IConditionPrimitives[] = [
            {
                column: 'column "--! DROP tabla;"',
                value: 1,
                condition: '=',
            },
        ];
        expect(() => new SelectAgregator(columns, from, undefined, whereAux)).toThrow(QueryFormatError);
        whereAux = [
            {
                column: 'column',
                value: 'Alter',
                condition: '=',
            },
        ];
        expect(() => new SelectAgregator(columns, from, undefined, whereAux)).toThrow(QueryFormatError);
        whereAux = [
            {
                column: 'column',
                value: ['Alter'],
                condition: '=',
            },
        ];
        expect(() => new SelectAgregator(columns, from, undefined, whereAux)).toThrow(QueryFormatError);
    });

    test('Crear una instancia Select con valor invalido en groupBy', () => {
        const groupByAux: IGroupByPrimitives = {
            by: ['id'],
            having: [
                {
                    column: 'column2',
                    value: ' :!? ',
                    condition: '=',
                },
            ],
        };
        expect(() => new SelectAgregator(columns, from, joins, where, groupByAux)).toThrow(QueryFormatError);
        groupByAux.having = [
            {
                column: 'DROP',
                value: ['John'],
                condition: '=',
            },
        ];
        expect(() => new SelectAgregator(columns, from, joins, where, groupByAux)).toThrow(QueryFormatError);
        groupByAux.having = [
            {
                column: 'DROP',
                value: 'John',
                condition: '=',
            },
        ];
        expect(() => new SelectAgregator(columns, from, joins, where, groupByAux)).toThrow(QueryFormatError);
        groupByAux.by.push('JOIN');
        expect(() => new SelectAgregator(columns, from, joins, where, groupByAux)).toThrow(QueryFormatError);
        groupByAux.by = [];
        expect(() => new SelectAgregator(columns, from, joins, where, groupByAux)).toThrow(QueryFormatError);
    });

    test('Crear una instancia Select con valor invalido en orderBy', () => {
        const orderByAux: IOrderByPrimitives = {
            orde: 'ASC',
            columns: ['!!'],
        };
        expect(() => new SelectAgregator(columns, from, joins, where, groupBy, limit, orderByAux)).toThrow(QueryFormatError);
    });
    test('Crear una instancia Insert y validar toprimitives', () => {
        const toPrimitives = new SelectAgregator(columns, from, joins, where, groupBy, limit, orderBy).toPrimitives();
        expect(toPrimitives).toEqual({ columns, from, joins, where, group: groupBy, limit, orderBy });
    });

    test('Ejecutar pipeline correctamente', () => {
        const agregator = new SelectAgregator(columns, from, joins, where, groupBy, limit, orderBy);
        const resultPipeline = agregator.runPipeline<[]>({
            data: [],
            affectedRows: 0,
        });
        expect(resultPipeline).toEqual([]);
    });

    test('Error en pipeline se espera error PipelineError', () => {
        const pipeHandler: IPipeHandler = {
            runPipe: jest.fn(() => {
                throw Error();
            }),
            canChannel: jest.fn(() => true),
        };
        const agregator = new SelectAgregator(columns, from, joins, where, groupBy, limit, orderBy);
        expect(() =>
            agregator.runPipeline(
                {
                    data: [],
                    affectedRows: 0,
                },
                [pipeHandler]
            )
        ).toThrow(PipelineError);
    });
});
