import { describe, expect, jest, test } from '@jest/globals';

import { InsertAggregator } from '../../../lib/insert/domain/insert.agregator';
import { PipelineError } from '../../../lib/shared/domain/error/pipeline.error';
import { QueryFormatError } from '../../../lib/shared/domain/error/query.format.error';

import { IAssignamentsPrimitives, IPipeHandler, ITablePrimitives } from '../../../lib/public.api';

describe('InsertAggregator', () => {
    const table: ITablePrimitives = 'pruebas';
    const assignments: IAssignamentsPrimitives = {
        col1: 'value1',
        col2: 'value2',
    };

    test('crear una instancia con valores validos y verificar que se instancian los value objects', () => {
        const insert = new InsertAggregator(table, assignments);
        expect(insert).toBeInstanceOf(InsertAggregator);
    });

    test('Crear una instancia InsertAggregator con valor invalido en table', () => {
        const tableAux: ITablePrimitives = '"1=1"';
        expect(() => new InsertAggregator(tableAux, assignments)).toThrow(QueryFormatError);
    });

    test('Crear una instancia InsertAggregator con valor invalido en assignments', () => {
        let assignmentsAux: IAssignamentsPrimitives = {
            col1: 'DROP',
        };
        expect(() => new InsertAggregator(table, assignmentsAux)).toThrow(QueryFormatError);
        assignmentsAux = {
            set: 'value1',
        };
        expect(() => new InsertAggregator(table, assignmentsAux)).toThrow(QueryFormatError);
    });

    test('Crear una instancia InsertAggregator y validar toprimitives', () => {
        const toPrimitives = new InsertAggregator(table, assignments).toPrimitives();
        expect(toPrimitives).toEqual({ table, assignments });
    });

    test('Ejecutar pipeline correctamente', () => {
        const agregator = new InsertAggregator(table, assignments);
        const resultPipeline = agregator.runPipeline({
            data: [],
            affectedRows: 0,
        });
        expect(resultPipeline).toEqual({ affectedRows: 0 });
    });

    test('Error en pipeline se espera error PipelineError', () => {
        const pipeHandler: IPipeHandler = {
            runPipe: jest.fn(() => {
                throw Error();
            }),
            canChannel: jest.fn(() => true),
        };
        const agregator = new InsertAggregator(table, assignments);
        expect(() =>
            agregator.runPipeline(
                {
                    data: [],
                    affectedRows: 0,
                },
                [pipeHandler]
            )
        ).toThrow(PipelineError);
    });
});
