import { describe, expect, test } from '@jest/globals';

import { InsertAggregator } from '../../../../lib/insert/domain/insert.agregator';
import { InsertSqlBuilderService } from '../../../../lib/insert/domain/services/sql.builder.service';

import { IAssignamentsPrimitives, ITablePrimitives } from '../../../../lib/public.api';

describe('InsertSqlBuilderService', () => {
    const table: ITablePrimitives = 'pruebas';
    const assignments: IAssignamentsPrimitives = {
        col1: 'value1',
        col2: 'value2',
    };

    test('Construir una sql valida con todos los campos', () => {
        const agregator = new InsertAggregator(table, assignments);
        expect(InsertSqlBuilderService.build(agregator, 'pr')).toEqual(`INSERT INTO pr_pruebas (col1, col2) VALUES (value1, value2);`);
    });
});
