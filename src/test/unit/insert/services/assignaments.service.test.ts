import { describe, expect, test } from '@jest/globals';

import { InsertAssignamentsService } from '../../../../lib/insert/domain/services/assignaments.service';

import { IAssignamentsPrimitives } from '../../../../lib/public.api';

describe('InsertAssignamentsService', () => {
    const assignments: IAssignamentsPrimitives = {
        col1: 'value1',
        col2: 'value2',
    };

    test('Contruir el string sql', () => {
        const result = InsertAssignamentsService.Build(assignments);
        const fromSql = ` (col1, col2) VALUES (value1, value2)`;
        expect(typeof result).toBe('string');
        expect(result).toBe(fromSql);
    });
});
