import { describe, expect, test } from '@jest/globals';

import { InsertTableService } from '../../../../lib/insert/domain/services/table.service';

import { ITablePrimitives } from '../../../../lib/public.api';

describe('InsertTableService', () => {
    const table: ITablePrimitives = 'pruebas';

    test('Contruir el string sql', () => {
        const result = InsertTableService.Build(table);
        const fromSql = `INSERT INTO pruebas`;
        expect(typeof result).toBe('string');
        expect(result).toBe(fromSql);
    });

    test('Contruir el string sql con prefijo', () => {
        const result = InsertTableService.Build(table, 'prefijo');
        const fromSql = `INSERT INTO prefijo_pruebas`;
        expect(typeof result).toBe('string');
        expect(result).toBe(fromSql);
    });
});
