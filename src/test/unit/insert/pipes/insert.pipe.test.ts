import { describe, expect, test } from '@jest/globals';

import { InsertPipe } from '../../../../lib/insert/domain/pipes/insert.pipe';

describe('InsertPipe', () => {
    test('Crear una instancia InsertPipe', () => {
        const instance = new InsertPipe();
        expect(instance).toBeInstanceOf(InsertPipe);
    });

    test('Al correr el pipeline se devuelve el resultado esperado', () => {
        const instance = new InsertPipe();
        expect(
            instance.runPipe({
                affectedRows: 0,
                data: [
                    {
                        col1: 'test 1',
                        col2: 'test 2',
                    },
                ],
            })
        ).toEqual({
            affectedRows: 0,
        });
    });
});
