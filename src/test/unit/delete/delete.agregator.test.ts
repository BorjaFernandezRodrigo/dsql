import { describe, expect, jest, test } from '@jest/globals';

import { DeleteAgregator } from '../../../lib/delete/domain/delete.agregator';
import { PipelineError } from '../../../lib/shared/domain/error/pipeline.error';
import { QueryFormatError } from '../../../lib/shared/domain/error/query.format.error';

import { IConditionPrimitives, IPipeHandler, ITablePrimitives } from '../../../lib/public.api';

describe('DeleteAgregator', () => {
    const table: ITablePrimitives = 'pruebas';
    const where: IConditionPrimitives[] = [
        {
            column: 'col1',
            value: '1',
            condition: '=',
        },
    ];

    test('crear una instancia con valores validos y verificar que se instancian los value objects', () => {
        const sql = new DeleteAgregator(table);
        expect(sql).toBeInstanceOf(DeleteAgregator);
    });

    test('Crear una instancia DeleteAgregator con valor invalido en table', () => {
        const tableAux: ITablePrimitives = '"1=1"';
        expect(() => new DeleteAgregator(tableAux, where)).toThrow(QueryFormatError);
    });

    test('Crear una instancia Insert y validar toprimitives', () => {
        const toPrimitives = new DeleteAgregator(table, where).toPrimitives();
        expect(toPrimitives).toEqual({ table, where });
    });

    test('Ejecutar pipeline correctamente', () => {
        const agregator = new DeleteAgregator(table, where);
        const resultPipeline = agregator.runPipeline({
            data: [],
            affectedRows: 0,
        });
        expect(resultPipeline).toEqual({ affectedRows: 0 });
    });

    test('Error en pipeline se espera error PipelineError', () => {
        const pipeHandler: IPipeHandler = {
            runPipe: jest.fn(() => {
                throw Error();
            }),
            canChannel: jest.fn(() => true),
        };
        const agregator = new DeleteAgregator(table, where);
        expect(() =>
            agregator.runPipeline(
                {
                    data: [],
                    affectedRows: 0,
                },
                [pipeHandler]
            )
        ).toThrow(PipelineError);
    });
});
