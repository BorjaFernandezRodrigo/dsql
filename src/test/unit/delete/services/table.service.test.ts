import { describe, expect, test } from '@jest/globals';

import { DeleteTableService } from '../../../../lib/delete/domain/services/table.service';

import { ITablePrimitives } from '../../../../lib/public.api';

describe('DeleteTableService', () => {
    const table: ITablePrimitives = 'pruebas';

    test('Contruir el string sql', () => {
        const result = DeleteTableService.Build(table);
        const fromSql = `DELETE FROM pruebas`;
        expect(typeof result).toBe('string');
        expect(result).toBe(fromSql);
    });

    test('Contruir el string sql con prefijo', () => {
        const result = DeleteTableService.Build(table, 'prefijo');
        const fromSql = `DELETE FROM prefijo_pruebas`;
        expect(typeof result).toBe('string');
        expect(result).toBe(fromSql);
    });
});
