import { describe, expect, test } from '@jest/globals';

import { DeleteAgregator } from '../../../../lib/delete/domain/delete.agregator';
import { DeleteSqlBuilderService } from '../../../../lib/delete/domain/services/sql.builder.service';

import { IConditionPrimitives, ITablePrimitives } from '../../../../lib/public.api';

describe('DeleteSqlBuilderService', () => {
    const table: ITablePrimitives = 'pruebas';
    const where: IConditionPrimitives[] = [
        {
            column: 'col1',
            value: '1',
            condition: '=',
        },
    ];

    test('Construir una sql valida con todos los campos', () => {
        const agregator = new DeleteAgregator(table, where);
        expect(DeleteSqlBuilderService.build(agregator, 'pr')).toEqual(`DELETE FROM pr_pruebas WHERE col1 = '1';`);
    });

    test('Construir una sql valida solo con campos obligatorios', () => {
        const agregator = new DeleteAgregator(table);
        expect(DeleteSqlBuilderService.build(agregator)).toEqual(`DELETE FROM pruebas;`);
    });
});
