import { describe, expect, test } from '@jest/globals';

import { UpdateSqlBuilderService } from '../../../../lib/update/domain/services/sql.builder.service';
import { UpdateAgregator } from '../../../../lib/update/domain/update.agregator';

import { IAssignamentsPrimitives, IConditionPrimitives, ITablePrimitives } from '../../../../lib/public.api';

describe('UpdateSqlBuilderService', () => {
    const table: ITablePrimitives = 'pruebas';
    const assignments: IAssignamentsPrimitives = {
        col1: 'value1',
        col2: 'value2',
    };
    const where: IConditionPrimitives[] = [
        {
            column: 'col1',
            value: '1',
            condition: '=',
        },
    ];

    test('Crear una instancia Insert y verificar el resultado de toSql()', () => {
        const agregator = new UpdateAgregator(table, assignments, where);
        expect(UpdateSqlBuilderService.build(agregator, 'pr')).toEqual(
            `UPDATE pr_pruebas SET col1 = 'value1', col2 = 'value2' WHERE col1 = '1';`
        );
    });

    test('Crear una instancia Update con valores minimos y verificar el resultado de toPrimitives();', () => {
        const agregator = new UpdateAgregator(table, assignments);
        expect(UpdateSqlBuilderService.build(agregator)).toEqual(`UPDATE pruebas SET col1 = 'value1', col2 = 'value2';`);
    });
});
