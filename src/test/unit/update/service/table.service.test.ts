import { describe, expect, test } from '@jest/globals';

import { UpdateTableService } from '../../../../lib/update/domain/services/table.service';

import { ITablePrimitives } from '../../../../lib/public.api';

describe('UpdateTableService', () => {
    const table: ITablePrimitives = 'pruebas';

    test('Contruir el string sql', () => {
        const result = UpdateTableService.Build(table);
        const fromSql = `UPDATE pruebas`;
        expect(typeof result).toBe('string');
        expect(result).toBe(fromSql);
    });

    test('Contruir el string sql con prefijo', () => {
        const result = UpdateTableService.Build(table, 'prefijo');
        const fromSql = `UPDATE prefijo_pruebas`;
        expect(typeof result).toBe('string');
        expect(result).toBe(fromSql);
    });
});
