import { describe, expect, test } from '@jest/globals';

import { UpdateAssignamentsService } from '../../../../lib/update/domain/services/assignaments.service';

import { IAssignamentsPrimitives } from '../../../../lib/public.api';

describe('UpdateAssignamentsService', () => {
    const assignments: IAssignamentsPrimitives = {
        col1: 'value1',
        col2: 'value2',
    };

    test('Construir una sql valida solo con campos obligatorios', () => {
        const result = UpdateAssignamentsService.Build(assignments);
        const fromSql = ` SET col1 = 'value1', col2 = 'value2'`;
        expect(typeof result).toBe('string');
        expect(result).toBe(fromSql);
    });
});
