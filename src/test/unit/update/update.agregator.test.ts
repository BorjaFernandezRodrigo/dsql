import { describe, expect, jest, test } from '@jest/globals';

import { PipelineError } from '../../../lib/shared/domain/error/pipeline.error';
import { QueryFormatError } from '../../../lib/shared/domain/error/query.format.error';
import { UpdateAgregator } from '../../../lib/update/domain/update.agregator';

import { IAssignamentsPrimitives, IConditionPrimitives, IPipeHandler, ITablePrimitives } from '../../../lib/public.api';

describe('UpdateAgregator', () => {
    const table: ITablePrimitives = 'pruebas';
    const assignments: IAssignamentsPrimitives = {
        col1: 'value1',
        col2: 'value2',
    };
    const where: IConditionPrimitives[] = [
        {
            column: 'col1',
            value: '1',
            condition: '=',
        },
    ];

    test('crear una instancia con valores validos y verificar que se instancian los value objects', () => {
        const update = new UpdateAgregator(table, assignments, where);
        expect(update).toBeInstanceOf(UpdateAgregator);
    });

    test('Crear una instancia UpdateAgregator con valor invalido en table', () => {
        const tableAux: ITablePrimitives = '"1=1"';
        expect(() => new UpdateAgregator(tableAux, assignments, where)).toThrow(QueryFormatError);
    });

    test('Crear una instancia UpdateAgregator con valor invalido en assignments', () => {
        let assignmentsAux: IAssignamentsPrimitives = {
            col1: 'DROP',
        };
        expect(() => new UpdateAgregator(table, assignmentsAux, where)).toThrow(QueryFormatError);
        assignmentsAux = {
            set: 'value1',
        };
        expect(() => new UpdateAgregator(table, assignmentsAux, where)).toThrow(QueryFormatError);
    });

    test('Crear una instancia UpdateAgregator con valor invalido en where', () => {
        let whereAux: IConditionPrimitives[] = [
            {
                column: 'column "--! DROP tabla;"',
                value: 1,
                condition: '=',
            },
        ];
        expect(() => new UpdateAgregator(table, assignments, whereAux)).toThrow(QueryFormatError);
        whereAux = [
            {
                column: 'column',
                value: 'Alter',
                condition: '=',
            },
        ];
        expect(() => new UpdateAgregator(table, assignments, whereAux)).toThrow(QueryFormatError);
        whereAux = [
            {
                column: 'column',
                value: ['Alter'],
                condition: '=',
            },
        ];
        expect(() => new UpdateAgregator(table, assignments, whereAux)).toThrow(QueryFormatError);
    });

    test('Crear una instancia Insert y validar toprimitives', () => {
        const toPrimitives = new UpdateAgregator(table, assignments, where).toPrimitives();
        expect(toPrimitives).toEqual({ table, assignments, where });
    });

    test('Lanzar el pipeline sin pipes y sin result', () => {
        const instance = new UpdateAgregator(table, assignments, where);
        expect(
            instance.runPipeline({
                affectedRows: 0,
                data: [],
            })
        ).toEqual({
            affectedRows: 0,
        });
    });

    test('Ejecutar pipeline correctamente', () => {
        const agregator = new UpdateAgregator(table, assignments, where);
        const resultPipeline = agregator.runPipeline({
            data: [],
            affectedRows: 0,
        });
        expect(resultPipeline).toEqual({ affectedRows: 0 });
    });

    test('Error en pipeline se espera error PipelineError', () => {
        const pipeHandler: IPipeHandler = {
            runPipe: jest.fn(() => {
                throw Error();
            }),
            canChannel: jest.fn(() => true),
        };
        const agregator = new UpdateAgregator(table, assignments, where);
        expect(() =>
            agregator.runPipeline(
                {
                    data: [],
                    affectedRows: 0,
                },
                [pipeHandler]
            )
        ).toThrow(PipelineError);
    });
});
