import { describe, expect, test } from '@jest/globals';

import { UpdatePipe } from '../../../../lib/update/domain/pipes/update.pipe';

describe('UpdatePipe', () => {
    test('Crear una instancia UpdatePipe', () => {
        const instance = new UpdatePipe();
        expect(instance).toBeInstanceOf(UpdatePipe);
    });

    test('Al correr el pipeline se devuelve el resultado esperado', () => {
        const instance = new UpdatePipe();
        expect(
            instance.runPipe({
                affectedRows: 0,
                data: [
                    {
                        col1: 'test 1',
                        col2: 'test 2',
                    },
                ],
            })
        ).toEqual({
            affectedRows: 0,
        });
    });
});
