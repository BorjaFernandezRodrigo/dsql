import { beforeAll, describe, expect, test } from '@jest/globals';
import { zip } from 'rxjs';

import { DSql, MySqlDriver, SqlLiteDriver } from '../../lib/public.api';
import { environment } from '../environment';
import { ExecQuery } from '../tools/query.exec';

describe('DSql', () => {
    beforeAll((done) => {
        const execQuery = new ExecQuery();
        zip([execQuery.mysql(`CREATE DATABASE IF NOT EXISTS dslq_tests; USE dslq_tests;`)]).subscribe({
            next: () => {
                done();
            },
            error: () => {
                done();
            },
        });
    });

    test('Crear una instancia DSql con driver mysql', () => {
        const driver = new MySqlDriver(environment.mysql);
        const instance = new DSql(driver, 'pr');
        expect(instance).toBeInstanceOf(DSql);
    });

    test('Crear una instancia DSql con driver sqlite', () => {
        const driver = new SqlLiteDriver(environment.mysql);
        const instance = new DSql(driver, 'pr');
        expect(instance).toBeInstanceOf(DSql);
    });
});
