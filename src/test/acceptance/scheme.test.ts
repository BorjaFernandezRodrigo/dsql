/* eslint-disable max-len */
import { beforeAll, describe, expect, test } from '@jest/globals';
import { zip } from 'rxjs';

import { ISchemeResult } from '../../lib/scheme/domain/types/result.type';

import { DSql, IPrefixPrimitive, ITablePrimitives, MySqlDriver, SqlLiteDriver } from '../../lib/public.api';
import { environment } from '../environment';
import { ExecQuery } from '../tools/query.exec';

describe('DSql - Scheme', () => {
    const table: ITablePrimitives = 'select_tests1';
    const prefix: IPrefixPrimitive = 'pr';

    beforeAll((done) => {
        const execQuery = new ExecQuery();
        zip([
            execQuery.mysql(` 
                    DROP DATABASE IF EXISTS dslq_tests;
                    CREATE DATABASE IF NOT EXISTS dslq_tests;
                    USE dslq_tests;
                    DROP TABLE IF EXISTS pr_select_tests1;
                    CREATE TABLE IF NOT EXISTS pr_select_tests1 (char_value char(50) COLLATE armscii8_bin DEFAULT NULL,int_value int(11) DEFAULT NULL,json_value longtext COLLATE armscii8_bin DEFAULT NULL,date_value date DEFAULT NULL,datetime_value datetime DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;
                `),

            execQuery.sqLite([
                'DROP TABLE IF EXISTS pr_select_tests1;',
                'CREATE TABLE IF NOT EXISTS pr_select_tests1 (char_value char(50) DEFAULT NULL,int_value int(11) DEFAULT NULL,json_value longtext DEFAULT NULL,date_value date DEFAULT NULL,datetime_value datetime DEFAULT NULL);',
            ]),
        ]).subscribe({
            next: () => {
                done();
            },
            error: () => {
                done();
            },
        });
    });

    test('Consultar Scheme de una tabla y confirmar el resultado y conexión a base de datos Mysql', (done) => {
        const driver = new MySqlDriver(environment.mysql);
        const instance = new DSql(driver, prefix);
        instance.scheme(table).subscribe({
            next: (result: ISchemeResult[]) => {
                expect(result).toEqual([
                    {
                        column: 'char_value',
                        type: 'char',
                        nullable: true,
                        primary: false,
                        defaulValue: undefined,
                    },
                    {
                        column: 'int_value',
                        type: 'int',
                        nullable: true,
                        primary: false,
                        defaulValue: undefined,
                    },
                    {
                        column: 'json_value',
                        type: 'longtext',
                        nullable: true,
                        primary: false,
                        defaulValue: undefined,
                    },
                    {
                        column: 'date_value',
                        type: 'date',
                        nullable: true,
                        primary: false,
                        defaulValue: undefined,
                    },
                    {
                        column: 'datetime_value',
                        type: 'datetime',
                        nullable: true,
                        primary: false,
                        defaulValue: undefined,
                    },
                ]);
            },
            error: (err) => {
                throw Error(err);
            },
            complete: () => {
                done();
            },
        });
    });

    test('Consultar Scheme de una tabla y confirmar el resultado y conexión a base de datos SQlite', (done) => {
        const driver = new SqlLiteDriver(environment.sqlite);
        const instance = new DSql(driver, prefix);
        instance.scheme(table).subscribe({
            next: (result) => {
                expect(result).toEqual([
                    {
                        column: 'char_value',
                        type: 'char(50)',
                        nullable: false,
                        primary: false,
                        defaulValue: undefined,
                    },
                    {
                        column: 'int_value',
                        type: 'int(11)',
                        nullable: false,
                        primary: false,
                        defaulValue: undefined,
                    },
                    {
                        column: 'json_value',
                        type: 'longtext',
                        nullable: false,
                        primary: false,
                        defaulValue: undefined,
                    },
                    {
                        column: 'date_value',
                        type: 'date',
                        nullable: false,
                        primary: false,
                        defaulValue: undefined,
                    },
                    {
                        column: 'datetime_value',
                        type: 'datetime',
                        nullable: false,
                        primary: false,
                        defaulValue: undefined,
                    },
                ]);
            },
            error: (err) => {
                throw Error(err);
            },
            complete: () => {
                done();
            },
        });
    });
});
