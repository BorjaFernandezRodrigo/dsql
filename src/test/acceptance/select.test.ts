/* eslint-disable max-len */
import { beforeAll, describe, expect, test } from '@jest/globals';
import moment from 'moment';
import { zip } from 'rxjs';

import { DSql, ISelectPrimitives, MySqlDriver, SqlLiteDriver } from '../../lib/public.api';
import { environment } from '../environment';
import { ExecQuery } from '../tools/query.exec';

describe('DSql - Select', () => {
    type IResult = {
        Col1: string;
        Col2: 1;
        Col3: typeof moment;
    };

    beforeAll((done) => {
        const execQuery = new ExecQuery();
        zip([
            execQuery.mysql(` 
                    DROP DATABASE IF EXISTS dslq_tests;
                    CREATE DATABASE IF NOT EXISTS dslq_tests;
                    USE dslq_tests;
                    DROP TABLE IF EXISTS pr_select_tests1;
                    CREATE TABLE IF NOT EXISTS pr_select_tests1 (char_value char(50) COLLATE armscii8_bin DEFAULT NULL,int_value int(11) DEFAULT NULL,json_value longtext COLLATE armscii8_bin DEFAULT NULL,date_value date DEFAULT NULL,datetime_value datetime DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;
                    INSERT INTO pr_select_tests1 (char_value, int_value, json_value, date_value, datetime_value) VALUES ('text', 1, '{"a": "1"}', '2021-11-02', '2021-11-02 09:11:30');
                    DROP TABLE IF EXISTS pr_select_tests2;
                    CREATE TABLE IF NOT EXISTS pr_select_tests2 (char_value char(50) COLLATE armscii8_bin DEFAULT NULL,int_value int(11) DEFAULT NULL,json_value longtext COLLATE armscii8_bin DEFAULT NULL,date_value date DEFAULT NULL,datetime_value datetime DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin ROW_FORMAT=DYNAMIC;
                    INSERT INTO pr_select_tests2 (char_value, int_value, json_value, date_value, datetime_value) VALUES ('text', 1, '{"a": "1"}', '2021-11-02', '2021-11-02 09:11:30');
                    DROP TABLE IF EXISTS pr_select_tests3;
                    CREATE TABLE IF NOT EXISTS pr_select_tests3 (char_value char(50) COLLATE armscii8_bin DEFAULT NULL,int_value int(11) DEFAULT NULL,json_value longtext COLLATE armscii8_bin DEFAULT NULL,date_value date DEFAULT NULL,datetime_value datetime DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin ROW_FORMAT=DYNAMIC;
                    INSERT INTO pr_select_tests3 (char_value, int_value, json_value, date_value, datetime_value) VALUES ('text', 1, '{"a": "1"}', '2021-11-02', '2021-11-02 09:11:30');
                `),

            execQuery.sqLite([
                'DROP TABLE IF EXISTS pr_select_tests1;',
                'CREATE TABLE IF NOT EXISTS pr_select_tests1 (char_value char(50) DEFAULT NULL,int_value int(11) DEFAULT NULL,json_value longtext DEFAULT NULL,date_value date DEFAULT NULL,datetime_value datetime DEFAULT NULL);',
                'INSERT INTO pr_select_tests1 (char_value, int_value, json_value, date_value, datetime_value) VALUES ("text", 1, "{""a"" ""1""}","2021-11-02", "2021-11-02 09:11:30");',
                'DROP TABLE IF EXISTS pr_select_tests2;',
                'CREATE TABLE IF NOT EXISTS pr_select_tests2 (char_value char(50) DEFAULT NULL,int_value int(11) DEFAULT NULL,json_value longtext DEFAULT NULL,date_value date DEFAULT NULL,datetime_value datetime DEFAULT NULL);',
                'INSERT INTO pr_select_tests2 (char_value, int_value, json_value, date_value, datetime_value) VALUES ("text", 1, "{""a"" ""1""}", "2021-11-02", "2021-11-02 09:11:30");',
                'DROP TABLE IF EXISTS pr_select_tests3;',
                'CREATE TABLE IF NOT EXISTS pr_select_tests3 (char_value char(50) DEFAULT NULL,int_value int(11) DEFAULT NULL,json_value longtext DEFAULT NULL,date_value date DEFAULT NULL,datetime_value datetime DEFAULT NULL) ;',
                'INSERT INTO pr_select_tests3 (char_value, int_value, json_value, date_value, datetime_value) VALUES ("text", 1, "{""a"" ""1""}", "2021-11-02", "2021-11-02 09:11:30");',
            ]),
        ]).subscribe({
            next: () => {
                done();
            },
            error: () => {
                done();
            },
        });
    });

    const sql: ISelectPrimitives = {
        columns: {
            'table1.char_value': {
                alias: 'Col1',
                type: 'string',
            },
            'table1.int_value': {
                alias: 'Col2',
                type: 'int',
            },
            'table1.datetime_value': {
                alias: 'Col3',
                type: 'moment',
            },
            'table1.date_value': {
                alias: 'Col4',
                type: 'moment',
            },
        },
        from: { 'select_tests1 table1': {} },
        joins: [
            {
                clausule: '><',
                columnsUnion: {
                    'table1.char_value': 'table2.char_value',
                },
                from: { 'select_tests2 table2': {} },
            },
            {
                clausule: '><',
                columnsUnion: {
                    'table1.char_value': 'table3.char_value',
                },
                from: { select_tests3: { alias: 'table3' } },
            },
        ],
        where: [
            {
                column: 'table1.int_value',
                value: [1, 2],
                condition: 'between',
            },
            {
                column: 'table1.char_value',
                condition: 'in',
                value: ['text'],
            },
            {
                column: 'table1.int_value',
                condition: '=',
                value: 1,
            },
        ],
        group: {
            by: ['Col1', 'Col2', 'COl3', 'COl4'],
            having: [
                {
                    column: 'Col1',
                    condition: '=',
                    value: 'text',
                },
            ],
        },
        limit: 1,
    };

    test('Realizar una select y confirmar el resultado y conexión a base de datos Mysql', (done) => {
        const driver = new MySqlDriver(environment.mysql);
        const instance = new DSql(driver, 'pr');
        instance.select<IResult>(sql).subscribe({
            next: (result: IResult[]) => {
                expect(result).toEqual([
                    {
                        Col1: 'text',
                        Col2: 1,
                        Col3: moment('2021-11-02T09:11:30.000Z'),
                        Col4: moment('2021-11-02T00:00:00.000Z'),
                    },
                ]);
            },
            error: (err) => {
                throw Error(err);
            },
            complete: () => {
                done();
            },
        });
    });

    test('Realizar una select y confirmar el resultado y conexión a base de datos SQlite', (done) => {
        const driver = new SqlLiteDriver(environment.sqlite);
        const instance = new DSql(driver, 'pr');
        instance.select<IResult>(sql).subscribe({
            next: (result) => {
                expect(result).toEqual([
                    {
                        Col1: 'text',
                        Col2: 1,
                        Col3: moment('2021-11-02 09:11:30'),
                        Col4: moment('2021-11-02'),
                    },
                ]);
            },
            error: (err) => {
                throw Error(err);
            },
            complete: () => {
                done();
            },
        });
    });
});
