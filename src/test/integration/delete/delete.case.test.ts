import { describe, expect, jest, test } from '@jest/globals';
import { of, throwError } from 'rxjs';

import { ExecRepositoryError } from '../../../lib/shared/domain/error/exec.repository.error';

import { DeleteCase } from '../../../lib/delete/aplication/delete.case';

import { IConditionPrimitives, IDeleteResult, IDriver, ITablePrimitives } from '../../../lib/public.api';

describe('DeleteCase', () => {
    const driver: IDriver = {
        query: jest.fn(() => of({ affectedRows: 0, data: [] })),
        schemTable: jest.fn(() => of()),
    };
    test('Crear una instancia de DeleteCase', () => {
        const instance = new DeleteCase(driver);
        expect(instance).toBeInstanceOf(DeleteCase);
    });

    test('Comprobar resultado correcto', (done) => {
        const instance = new DeleteCase(driver);
        const table: ITablePrimitives = 'pruebas';
        const where: IConditionPrimitives[] = [
            {
                column: 'col1',
                value: '1',
                condition: '=',
            },
        ];
        instance.run({ where, table }).subscribe({
            next: (result: IDeleteResult) => {
                expect(result.affectedRows).toBe(0);
                done();
            },
        });
    });

    test('Comprobar sql correcta', (done) => {
        const instance = new DeleteCase(driver);
        const table: ITablePrimitives = 'pruebas';
        const where: IConditionPrimitives[] = [
            {
                column: 'col1',
                value: '1',
                condition: '=',
            },
        ];
        instance.run({ where, table }).subscribe({
            next: () => {
                expect(driver.query).toBeCalledWith(`DELETE FROM pruebas WHERE col1 = '1';`);
                done();
            },
        });
    });

    test('Devuelve tipo de error ExecRepositoryError', (done) => {
        const errorDriver: IDriver = {
            query: jest.fn(() => throwError(() => new Error())),
            schemTable: jest.fn(() => of()),
        };
        const instance = new DeleteCase(errorDriver);
        const table: ITablePrimitives = 'pruebas';
        const where: IConditionPrimitives[] = [
            {
                column: 'col1',
                value: '1',
                condition: '=',
            },
        ];
        instance.run({ where, table }).subscribe({
            error: (err) => {
                expect(err).toBeInstanceOf(ExecRepositoryError);
                done();
            },
        });
    });
});
