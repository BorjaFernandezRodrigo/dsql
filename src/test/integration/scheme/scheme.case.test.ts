import { describe, expect, jest, test } from '@jest/globals';
import { of, throwError } from 'rxjs';

import { ISchemeResult } from '../../../lib/scheme/domain/types/result.type';
import { ExecRepositoryError } from '../../../lib/shared/domain/error/exec.repository.error';

import { SchemeCase } from '../../../lib/scheme/aplication/scheme.case';

import { IDriver, IPrefixPrimitive, ITablePrimitives } from '../../../lib/public.api';

describe('SchemeCase', () => {
    const table: ITablePrimitives = 'pruebas';
    const prefix: IPrefixPrimitive = 'pr';

    const driver: IDriver = {
        query: jest.fn(() => of()),
        schemTable: jest.fn(() => of([])),
    };

    test('Crear una instancia de SchemeCase', () => {
        const instance = new SchemeCase(driver);
        expect(instance).toBeInstanceOf(SchemeCase);
    });

    test('Comprobar resultado correcto', (done) => {
        const instance = new SchemeCase(driver);
        instance.run(table, prefix).subscribe({
            next: (result: ISchemeResult[]) => {
                expect(result).toEqual([]);
                done();
            },
        });
    });

    test('Comprobar sql correcta', (done) => {
        const instance = new SchemeCase(driver);
        instance.run(table, prefix).subscribe({
            next: () => {
                expect(driver.schemTable).toBeCalledWith(`pr_pruebas`);
                done();
            },
        });
    });

    test('Comprobar sql correcta sin prefijo', (done) => {
        const instance = new SchemeCase(driver);
        instance.run(table).subscribe({
            next: () => {
                expect(driver.schemTable).toBeCalledWith(`pruebas`);
                done();
            },
        });
    });

    test('Devuelve tipo de error ExecRepositoryError', (done) => {
        const errorDriver: IDriver = {
            schemTable: jest.fn(() => throwError(() => new Error())),
            query: jest.fn(() => of()),
        };
        const instance = new SchemeCase(errorDriver);
        instance.run(table, prefix).subscribe({
            error: (err) => {
                expect(err).toBeInstanceOf(ExecRepositoryError);
                done();
            },
        });
    });
});
