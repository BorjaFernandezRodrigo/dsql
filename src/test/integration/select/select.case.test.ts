import { describe, expect, jest, test } from '@jest/globals';
import { of, throwError } from 'rxjs';

import { ExecRepositoryError } from '../../../lib/shared/domain/error/exec.repository.error';

import { SelectCase } from '../../../lib/select/aplication/select.case';

import {
    IColumnsPrimitives,
    IConditionPrimitives,
    IDriver,
    IFromPrimitives,
    IGroupByPrimitives,
    IJoinPrimitives,
    ILimitPrimitives,
    IOrderByPrimitives,
} from '../../../lib/public.api';

describe('SelectCase', () => {
    const columns: IColumnsPrimitives = {
        id: { type: 'int', alias: 'ID' },
        name: { type: 'string' },
    };
    const from: IFromPrimitives = {
        table1: { alias: 't' },
    };
    const joins: IJoinPrimitives[] = [
        {
            clausule: '<',
            from: {
                table2: { alias: 't2' },
            },
            columnsUnion: {
                id: 't.id',
                name: 't.name',
            },
        },
    ];
    const where: IConditionPrimitives[] = [
        {
            column: 'id',
            value: 1,
            condition: '=',
        },
    ];
    const groupBy: IGroupByPrimitives = {
        by: ['id'],
        having: [
            {
                column: 'name',
                value: 'John',
                condition: '=',
            },
        ],
    };
    const limit: ILimitPrimitives = 10;
    const orderBy: IOrderByPrimitives = {
        orde: 'ASC',
        columns: ['name'],
    };

    const driver: IDriver = {
        query: jest.fn(() => of({ affectedRows: 0, data: [{ id: '1', name: 'pruebas' }] })),
        schemTable: jest.fn(() => of()),
    };

    test('Crear una instancia de SelectCase', () => {
        const instance = new SelectCase(driver);
        expect(instance).toBeInstanceOf(SelectCase);
    });

    test('Comprobar resultado correcto', (done) => {
        const instance = new SelectCase(driver);
        instance.run({ columns, from, joins, where, group: groupBy, limit, orderBy }).subscribe({
            next: (result) => {
                expect(result).toStrictEqual([{ id: '1', name: 'pruebas' }]);
                done();
            },
        });
    });

    test('Comprobar sql correcta', (done) => {
        const instance = new SelectCase(driver);
        instance.run({ columns, from, joins, where, group: groupBy, limit, orderBy }, 'pr').subscribe({
            next: () => {
                expect(driver.query).toBeCalledWith(
                    // eslint-disable-next-line max-len
                    `SELECT id AS 'ID', name FROM pr_table1 AS t LEFT JOIN pr_table2 AS t2 ON id = t.id AND name = t.name WHERE id = '1' GROUP BY id HAVING name = 'John' LIMIT 10 ORDER BY name ASC;`
                );
                done();
            },
        });
    });

    test('Devuelve tipo de error ExecRepositoryError', (done) => {
        const errorDriver: IDriver = {
            query: jest.fn(() => throwError(() => new Error())),
            schemTable: jest.fn(() => of()),
        };
        const instance = new SelectCase(errorDriver);
        instance.run({ columns, from, joins, where, group: groupBy, limit, orderBy }, 'pr').subscribe({
            error: (err) => {
                expect(err).toBeInstanceOf(ExecRepositoryError);
                done();
            },
        });
    });
});
