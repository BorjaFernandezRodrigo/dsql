import { describe, expect, jest, test } from '@jest/globals';
import { of, throwError } from 'rxjs';

import { ExecRepositoryError } from '../../../lib/shared/domain/error/exec.repository.error';

import { UpdateCase } from '../../../lib/update/aplication/update.case';

import { IAssignamentsPrimitives, IDeleteResult, IDriver, ITablePrimitives } from '../../../lib/public.api';

describe('UpdateCase', () => {
    test('Crear una instancia de UpdateCase', () => {
        const driver: IDriver = {
            query: jest.fn(() => of({ affectedRows: 0, data: [] })),
            schemTable: jest.fn(() => of()),
        };
        const instance = new UpdateCase(driver);
        expect(instance).toBeInstanceOf(UpdateCase);
    });

    test('Comprobar resultado correcto', (done) => {
        const driver: IDriver = {
            query: jest.fn(() => of({ affectedRows: 0, data: [] })),
            schemTable: jest.fn(() => of()),
        };
        const instance = new UpdateCase(driver);
        const table: ITablePrimitives = 'pruebas';
        const assignments: IAssignamentsPrimitives = {
            col1: 'value1',
            col2: 'value2',
        };
        instance.run({ assignments, table }).subscribe({
            next: (result: IDeleteResult) => {
                expect(result.affectedRows).toBe(0);
                done();
            },
        });
    });

    test('Comprobar sql correcta', (done) => {
        const driver: IDriver = {
            query: jest.fn(() => of({ affectedRows: 0, data: [] })),
            schemTable: jest.fn(() => of()),
        };
        const instance = new UpdateCase(driver);
        const table: ITablePrimitives = 'pruebas';
        const assignments: IAssignamentsPrimitives = {
            col1: 'value1',
            col2: 'value2',
        };
        instance.run({ assignments, table }).subscribe({
            next: () => {
                expect(driver.query).toBeCalledWith(`UPDATE pruebas SET col1 = 'value1', col2 = 'value2';`);
                done();
            },
        });
    });

    test('Devuelve tipo de error ExecRepositoryError', (done) => {
        const errorDriver: IDriver = {
            query: jest.fn(() => throwError(() => new Error())),
            schemTable: jest.fn(() => of()),
        };
        const instance = new UpdateCase(errorDriver);
        const table: ITablePrimitives = 'pruebas';
        const assignments: IAssignamentsPrimitives = {
            col1: 'value1',
            col2: 'value2',
        };
        instance.run({ assignments, table }).subscribe({
            error: (err) => {
                expect(err).toBeInstanceOf(ExecRepositoryError);
                done();
            },
        });
    });
});
