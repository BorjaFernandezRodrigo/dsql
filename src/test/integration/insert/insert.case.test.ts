import { describe, expect, jest, test } from '@jest/globals';
import { of, throwError } from 'rxjs';

import { ExecRepositoryError } from '../../../lib/shared/domain/error/exec.repository.error';

import { InsertCase } from '../../../lib/insert/aplication/insert.case';

import { IAssignamentsPrimitives, IDeleteResult, IDriver, ITablePrimitives } from '../../../lib/public.api';

describe('InsertCase', () => {
    const driver: IDriver = {
        query: jest.fn(() => of({ affectedRows: 0, data: [] })),
        schemTable: jest.fn(() => of()),
    };

    test('Crear una instancia de InsertCase', () => {
        const instance = new InsertCase(driver);
        expect(instance).toBeInstanceOf(InsertCase);
    });

    test('Comprobar resultado correcto', (done) => {
        const instance = new InsertCase(driver);
        const table: ITablePrimitives = 'pruebas';
        const assignments: IAssignamentsPrimitives = {
            col1: 'value1',
            col2: 'value2',
        };
        instance.run({ assignments, table }).subscribe({
            next: (result: IDeleteResult) => {
                expect(result.affectedRows).toBe(0);
                done();
            },
        });
    });

    test('Comprobar sql correcta', (done) => {
        const instance = new InsertCase(driver);
        const table: ITablePrimitives = 'pruebas';
        const assignments: IAssignamentsPrimitives = {
            col1: 'value1',
            col2: 'value2',
        };
        instance.run({ assignments, table }).subscribe({
            next: () => {
                expect(driver.query).toBeCalledWith(`INSERT INTO pruebas (col1, col2) VALUES (value1, value2);`);
                done();
            },
        });
    });

    test('Devuelve tipo de error ExecRepositoryError', (done) => {
        const errorDriver: IDriver = {
            query: jest.fn(() => throwError(() => new Error())),
            schemTable: jest.fn(() => of()),
        };
        const instance = new InsertCase(errorDriver);
        const table: ITablePrimitives = 'pruebas';
        const assignments: IAssignamentsPrimitives = {
            col1: 'value1',
            col2: 'value2',
        };
        instance.run({ assignments, table }).subscribe({
            error: (err) => {
                expect(err).toBeInstanceOf(ExecRepositoryError);
                done();
            },
        });
    });
});
