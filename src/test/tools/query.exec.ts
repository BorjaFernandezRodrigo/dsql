/* eslint-disable @typescript-eslint/no-explicit-any */

/* eslint-disable class-methods-use-this */
import mysql, { MysqlError } from 'mysql';
import { Observable } from 'rxjs';
import { Database } from 'sqlite3';

import { environment } from '../environment';

export class ExecQuery {
    public mysql(sql: string): Observable<any[]> {
        return new Observable((observer) => {
            const conector = mysql.createConnection({
                host: environment.mysql.host,
                password: environment.mysql.password,
                user: environment.mysql.user,
                port: environment.mysql.port ?? 3306,
                multipleStatements: true,
            });
            conector.query(sql, (error: MysqlError | undefined, results: any[]) => {
                if (error) observer.error(error);
                observer.next(results);
                observer.complete();
            });
            conector.commit();
            conector.end();
        });
    }

    public sqLite(sql: string[]): Observable<any[]> {
        return new Observable((observer) => {
            let conector: Database | undefined = new Database(environment.sqlite.database, (error) => {
                if (error) {
                    observer.error(`Error al conectar la sesión de base de datos. ${error}`);
                }
            });

            conector.serialize(() => {
                sql.forEach((statement: string) => conector?.run(`${statement};`));
                observer.next();
                observer.complete();
            });
            conector.close();
            conector = undefined;
        });
    }
}
