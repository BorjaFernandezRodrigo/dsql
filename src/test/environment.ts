import { join } from 'path';

import { IConfigDriverPrimitives } from '../lib/public.api';

export const environment: {
    mysql: IConfigDriverPrimitives;
    sqlite: IConfigDriverPrimitives;
} = {
    mysql: {
        host: process.env['MYSQL_HOST'] || '127.0.0.1',
        user: 'admin',
        password: 'abc123..',
        database: 'dslq_tests',
    },
    sqlite: {
        database: join(`${__dirname}/tools/dslq_tests.sqlite`),
    },
};
