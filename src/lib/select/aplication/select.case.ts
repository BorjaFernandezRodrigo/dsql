import { DsLogger } from '@desenrola/dslogger';
import { Observable, catchError, map, throwError } from 'rxjs';

import { ExecRepositoryError } from '../../shared/domain/error/exec.repository.error';
import { LoggerService } from '../../shared/domain/logger/loggerService';
import { IPipeHandler } from '../../shared/domain/pipe/types/pipe.handler.type';
import { IDriver } from '../../shared/domain/repository/driver.type';
import { IPrefixPrimitive } from '../../shared/domain/sql/types/prefix.type';
import { SelectAgregator } from '../domain/select.agregator';
import { SelectSqlBuilderService } from '../domain/services/sql.builder.service';
import { ISelectPrimitives } from '../domain/types/select.type';

export class SelectCase<T> {
    private repository: IDriver;

    private loggerService: LoggerService;

    constructor(repository: IDriver, logger?: DsLogger) {
        this.repository = repository;
        this.loggerService = new LoggerService(logger);
    }

    public run(request: ISelectPrimitives, prefix?: IPrefixPrimitive, pipes: IPipeHandler[] = []): Observable<T[]> {
        const startTime = Date.now();
        const agregator = this.buildAgregator(request);
        const sql = SelectSqlBuilderService.build(agregator, prefix);
        this.loggerService.addInfoLog(request, sql, prefix).printLog('debug', 'Se ejecuta Select');
        return this.repository.query(sql).pipe(
            catchError((error) => {
                this.loggerService.addError(error).printLog('error', 'Error en Select');
                return throwError(() => new ExecRepositoryError(error));
            }),
            map((result) => {
                const executionTime = Date.now() - startTime;
                this.loggerService.addResult(result).addTime(`${executionTime}ms`).printLog('info', 'Fin de Select');
                return agregator.runPipeline(result, pipes);
            })
        );
    }

    private buildAgregator(request: ISelectPrimitives) {
        return new SelectAgregator(
            request.columns,
            request.from,
            request.joins,
            request.where,
            request.group,
            request.limit,
            request.orderBy
        );
    }
}
