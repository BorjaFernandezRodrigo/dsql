import { PipeLineHandler } from '../../shared/domain/pipe/pipeline.handler';
import { IPipeLineHandler } from '../../shared/domain/pipe/types/pipeline.handler.type';

import { FormatSelectPipe } from './pipes/format.pipe';
import { SelectPipe } from './pipes/select.pipe';

export class SelectPipeline extends PipeLineHandler implements IPipeLineHandler {
    constructor() {
        super('select');
        this.addPipes([new SelectPipe(), new FormatSelectPipe()]);
    }
}
