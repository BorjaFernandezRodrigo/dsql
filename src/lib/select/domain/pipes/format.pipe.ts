/* eslint-disable @typescript-eslint/no-explicit-any */
import moment from 'moment';

import { IOperation } from '../../../shared/domain/pipe/types/operations.type';
import { IPipeHandler } from '../../../shared/domain/pipe/types/pipe.handler.type';
import { ISqlPrimitives } from '../../../shared/domain/primitives/aggregator';

import { ISelectPrimitives } from '../types/select.type';

export class FormatSelectPipe implements IPipeHandler {
    canChannel(operation: IOperation): boolean {
        return operation === 'select';
    }

    public runPipe(result: any[], sql: ISqlPrimitives) {
        const columnTypes = this.columnTypesBuilder(sql as ISelectPrimitives);
        if (Object.keys(columnTypes).length === 0) return result;
        return result?.map((row: any) => this.mapColumn(Object(row), columnTypes));
    }

    private columnTypesBuilder(sql: ISelectPrimitives): { [column: string]: string } {
        if (typeof sql.columns === 'string') return {};
        const columnTypes: { [key: string]: string } = {};
        const { columns } = sql;
        Object.keys(columns).forEach((colName) => {
            const col = columns[colName]?.alias ?? colName;
            columnTypes[col] = columns[colName]?.type ?? 'string';
        });
        return columnTypes;
    }

    private mapColumn(
        row: any[],
        columnTypes: {
            [column: string]: string;
        }
    ) {
        const newRow = Object(row);
        Object.keys(columnTypes).forEach((column: string) => {
            if (newRow[column]) {
                switch (columnTypes[column]) {
                    case 'int':
                        newRow[column] = Number(newRow[column]);
                        break;
                    case 'string':
                        newRow[column] = newRow[column].toString();
                        break;
                    case 'json':
                        newRow[column] = JSON.parse(newRow[column]);
                        break;
                    case 'boolean':
                        newRow[column] = JSON.parse(newRow[column].toLowerCase());
                        break;
                    case 'array':
                        newRow[column] = JSON.parse(newRow[column]);
                        break;
                    case 'date':
                        newRow[column] = new Date(newRow[column]);
                        break;
                    case 'moment':
                        newRow[column] = moment(newRow[column]);
                        break;
                    default:
                        break;
                }
            }
        });
        return newRow;
    }
}
