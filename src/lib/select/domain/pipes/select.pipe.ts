import { IOperation } from '../../../shared/domain/pipe/types/operations.type';
import { IPipeHandler } from '../../../shared/domain/pipe/types/pipe.handler.type';
import { IResultDriver } from '../../../shared/domain/repository/result-driver.type';

export class SelectPipe implements IPipeHandler {
    public runPipe(result: IResultDriver) {
        return result.data.map((row) => JSON.parse(JSON.stringify(row)));
    }

    public canChannel(operation: IOperation): boolean {
        return operation === 'select';
    }
}
