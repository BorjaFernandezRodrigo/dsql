import { PipelineError } from '../../shared/domain/error/pipeline.error';
import { QueryFormatError } from '../../shared/domain/error/query.format.error';
import { IPipeHandler } from '../../shared/domain/pipe/types/pipe.handler.type';
import { Agregator } from '../../shared/domain/primitives/aggregator';
import { IResultDriver } from '../../shared/domain/repository/result-driver.type';
import { IConditionPrimitives } from '../../shared/domain/sql/types/conditions.type';
import { WhereValueObject } from '../../shared/domain/sql/value-object/where.vo';

import { SelectPipeline } from './select.pipeline';
import { IColumnsPrimitives } from './types/columns.type';
import { IFromPrimitives } from './types/from.type';
import { IGroupByPrimitives } from './types/groupby.type';
import { IJoinPrimitives } from './types/joins.type';
import { ILimitPrimitives } from './types/limit.type';
import { IOrderByPrimitives } from './types/orderby.type';
import { ISelectPrimitives } from './types/select.type';
import { ColumnValueObject } from './value-object/columns.vo';
import { FromValueObject } from './value-object/from.vo';
import { GroupByValueObject } from './value-object/groupby.vo';
import { JoinValueObject } from './value-object/joins.vo';
import { LimitValueObject } from './value-object/limit.vo';
import { OrderByValueObject } from './value-object/orderby.vo';

export class SelectAgregator implements Agregator<ISelectPrimitives> {
    readonly columns: ColumnValueObject;

    readonly from?: FromValueObject;

    readonly joins?: JoinValueObject[];

    readonly where?: WhereValueObject;

    readonly limit?: LimitValueObject;

    readonly orderBy?: OrderByValueObject;

    readonly group?: GroupByValueObject;

    private pipeLine: SelectPipeline;

    constructor(
        columns: IColumnsPrimitives | '*',
        from?: IFromPrimitives,
        joins?: IJoinPrimitives[],
        where?: IConditionPrimitives[],
        group?: IGroupByPrimitives,
        limit?: ILimitPrimitives,
        orderBy?: IOrderByPrimitives
    ) {
        this.pipeLine = new SelectPipeline();
        try {
            this.columns = new ColumnValueObject(columns);
            this.from = from ? new FromValueObject(from) : undefined;
            this.joins = joins?.map((join) => new JoinValueObject(join));
            this.where = where ? new WhereValueObject(where) : undefined;
            this.limit = limit ? new LimitValueObject(limit) : undefined;
            this.orderBy = orderBy ? new OrderByValueObject(orderBy) : undefined;
            this.group = group ? new GroupByValueObject(group) : undefined;
        } catch (err) {
            throw new QueryFormatError(err);
        }
    }

    public runPipeline<R>(result: IResultDriver, pipes: IPipeHandler[] = []): R[] {
        try {
            return this.pipeLine.addPipes(pipes).runPipeLine(result, this.toPrimitives());
        } catch (error) {
            throw new PipelineError(error);
        }
    }

    public toPrimitives(): ISelectPrimitives {
        return {
            columns: this.columns.value,
            from: this.from?.value,
            joins: this.joins?.map((join) => join.value),
            where: this.where?.value,
            group: this.group?.value,
            limit: this.limit?.value,
            orderBy: this.orderBy?.value,
        };
    }
}
