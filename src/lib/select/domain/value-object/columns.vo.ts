import { SqlValueObject } from '../../../shared/domain/sql/value-object/sql.vo';

import { IColumnsPrimitives } from '../types/columns.type';

export class ColumnValueObject extends SqlValueObject<IColumnsPrimitives | '*'> {
    constructor(value: IColumnsPrimitives | '*') {
        super(value);
        this.validFormat(value);
    }

    public validFormat(value: IColumnsPrimitives | '*') {
        Object.keys(value).forEach((row) => {
            this.regexNotValid.forEach((regex) => {
                if (regex.test(row)) throw Error(`Formato columnas incorreto. value: ${JSON.stringify(value)}`);
            });
        });
    }
}
