import { SqlValueObject } from '../../../shared/domain/sql/value-object/sql.vo';

import { IGroupByPrimitives } from '../types/groupby.type';

export class GroupByValueObject extends SqlValueObject<IGroupByPrimitives> {
    constructor(value: IGroupByPrimitives) {
        super(value);
        this.validFormat(value);
    }

    public validFormat(value: IGroupByPrimitives) {
        if (value.by.length === 0) throw Error(`Formato group by incorreto. value: ${JSON.stringify(value)}`);
        value.by.forEach((row) => {
            this.regexNotValid.forEach((regex) => {
                if (regex.test(row)) throw Error(`Formato group by incorreto. value: ${JSON.stringify(value)}`);
            });
        });
        value.having.forEach((row) => {
            this.regexNotValid.forEach((regex) => {
                if (regex.test(row.column)) throw Error(`Formato incorreto. value: ${JSON.stringify(value)}`);
                if (typeof row.value === 'string' && regex.test(row.value))
                    throw Error(`Formato incorreto. value: ${JSON.stringify(value)}`);
                if (Array.isArray(row.value) && row.value.filter((item) => regex.test(item.toString())))
                    throw Error(`Formato incorreto. value: ${JSON.stringify(value)}`);
            });
        });
    }
}
