import { SqlValueObject } from '../../../shared/domain/sql/value-object/sql.vo';

import { IOrderByPrimitives } from '../types/orderby.type';

export class OrderByValueObject extends SqlValueObject<IOrderByPrimitives> {
    constructor(value: IOrderByPrimitives) {
        super(value);
        this.validFormat(value);
    }

    public validFormat(value: IOrderByPrimitives) {
        value.columns.forEach((row) => {
            this.regexNotValid.forEach((regex) => {
                if (regex.test(row)) throw Error(`Formato order by incorreto. value: ${row}`);
            });
        });
    }
}
