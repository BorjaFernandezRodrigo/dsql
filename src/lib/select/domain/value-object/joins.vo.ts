import { SqlValueObject } from '../../../shared/domain/sql/value-object/sql.vo';

import { IJoinPrimitives } from '../types/joins.type';

export class JoinValueObject extends SqlValueObject<IJoinPrimitives> {
    constructor(value: IJoinPrimitives) {
        super(value);
        this.validFormat(value);
    }

    public validFormat(value: IJoinPrimitives) {
        Object.keys(value.from).forEach((row) => {
            this.regexNotValid.forEach((regex) => {
                if (regex.test(row) || regex.test(value.from[row].alias ?? ''))
                    throw Error(`Formato join incorreto. value: ${JSON.stringify(value)}`);
            });
        });
        Object.keys(value.columnsUnion).forEach((row: string) => {
            this.regexNotValid.forEach((regex) => {
                if (regex.test(row) || regex.test(value.columnsUnion[row].toString()))
                    throw Error(`Formato join incorreto. value: ${JSON.stringify(value)}`);
            });
        });
    }
}
