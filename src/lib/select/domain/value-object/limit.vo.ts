import { SqlValueObject } from '../../../shared/domain/sql/value-object/sql.vo';

import { ILimitPrimitives } from '../types/limit.type';

export class LimitValueObject extends SqlValueObject<ILimitPrimitives> {
    public validFormat() {
        return true;
    }
}
