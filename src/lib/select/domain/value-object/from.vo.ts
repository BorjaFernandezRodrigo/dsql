import { SqlValueObject } from '../../../shared/domain/sql/value-object/sql.vo';

import { IFromPrimitives } from '../types/from.type';

export class FromValueObject extends SqlValueObject<IFromPrimitives> {
    constructor(value: IFromPrimitives) {
        super(value);
        this.validFormat(value);
    }

    public validFormat(value: IFromPrimitives) {
        Object.keys(value).forEach((row) => {
            this.regexNotValid.forEach((regex) => {
                if (regex.test(row)) throw Error(`Formato from incorreto. value: ${JSON.stringify(value)}`);
            });
        });
    }
}
