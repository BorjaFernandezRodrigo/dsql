import { IPrefixPrimitive } from '../../../shared/domain/sql/types/prefix.type';

import { IFromPrimitives } from '../types/from.type';
import { IJoinPrimitives } from '../types/joins.type';

export abstract class JoinSelectService {
    private static readonly joinClausule = {
        '<': 'LEFT JOIN',
        '>': 'RIGHT JOIN',
        '<>': 'JOIN',
        '><': 'INNER JOIN',
    };

    public static Build(joins?: IJoinPrimitives[], prefix?: IPrefixPrimitive): string {
        if (!joins) return '';
        let sqlJoin = '';
        joins.forEach((join) => {
            sqlJoin += ` ${this.joinClausule[join.clausule]} ${this.tableBuild(join.from, prefix)} ON `;
            sqlJoin += this.joinBuilder(join.columnsUnion);
        });
        return sqlJoin;
    }

    private static tableBuild(from: IFromPrimitives, prefix?: IPrefixPrimitive): string {
        const formMap: string[] = Object.keys(from).map((key) => {
            const table = prefix ? `${prefix}_${key}` : key;
            const alias = from[key].alias ? `AS ${from[key].alias}` : '';
            return `${table} ${alias}`;
        });
        return formMap.toString();
    }

    public static joinBuilder(assignments: { [colum: string]: string | number }): string {
        let sqlJoin: string = ``;
        Object.keys(assignments).forEach((column, index) => {
            sqlJoin += index === 0 ? `${column} = ${assignments[column]}` : ` AND ${column} = ${assignments[column]}`;
        });
        return sqlJoin;
    }
}
