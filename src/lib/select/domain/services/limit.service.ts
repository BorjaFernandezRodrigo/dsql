import { ILimitPrimitives } from '../types/limit.type';

export abstract class LimitSelectService {
    public static Build(limit?: ILimitPrimitives): string {
        if (!limit) return '';
        return ` LIMIT ${limit}`;
    }
}
