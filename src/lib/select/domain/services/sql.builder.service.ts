import { ConditionsService } from '../../../shared/domain/sql/services/conditions.service';
import { IPrefixPrimitive } from '../../../shared/domain/sql/types/prefix.type';

import { SelectAgregator } from '../select.agregator';
import { ColumnsSelectService } from './columns.service';
import { FromSelectService } from './from.service';
import { GroupBySelectService } from './groupby.service';
import { JoinSelectService } from './joins.service';
import { LimitSelectService } from './limit.service';
import { OrderBySelectService } from './orderby.service';

export abstract class SelectSqlBuilderService {
    public static build(entity: SelectAgregator, prefix?: IPrefixPrimitive): string {
        const sqlColumns = ColumnsSelectService.Build(entity.columns.value);
        const sqlFrom = FromSelectService.Build(entity.from?.value, prefix);
        const sqlJoins = JoinSelectService.Build(
            entity.joins?.map((join) => join.value),
            prefix
        );
        const sqlWhere = ConditionsService.Build('WHERE', entity.where?.value);
        const sqlGroup = GroupBySelectService.Build(entity.group?.value);
        const sqlLimit = LimitSelectService.Build(entity.limit?.value);
        const sqlrderBy = OrderBySelectService.Build(entity.orderBy?.value);
        return `${sqlColumns}${sqlFrom}${sqlJoins}${sqlWhere}${sqlGroup}${sqlLimit}${sqlrderBy};`;
    }
}
