import { IColumnsPrimitives } from '../types/columns.type';

export abstract class ColumnsSelectService {
    public static Build(columns: IColumnsPrimitives | '*'): string {
        let sqlSelect: string = `SELECT `;
        if (columns === '*') return `${sqlSelect}*`;
        Object.keys(columns).forEach((colName, index) => {
            sqlSelect += index === 0 ? `${colName}` : `, ${colName}`;
            if (columns[colName]?.alias) {
                sqlSelect += ` AS '${columns[colName].alias}'`;
            }
        });
        return sqlSelect;
    }
}
