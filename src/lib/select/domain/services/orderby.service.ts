import { IOrderByPrimitives } from '../types/orderby.type';

export abstract class OrderBySelectService {
    public static Build(orderBy?: IOrderByPrimitives): string {
        if (!orderBy) return '';
        let sqlOrderBy = ` ORDER BY `;
        orderBy.columns.map((limit, index) => {
            sqlOrderBy += index === 0 ? `${limit}` : `, ${limit}`;
        });
        sqlOrderBy += ` ${orderBy.orde}`;
        return sqlOrderBy;
    }
}
