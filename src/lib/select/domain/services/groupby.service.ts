import { ConditionsService } from '../../../shared/domain/sql/services/conditions.service';
import { IConditionPrimitives } from '../../../shared/domain/sql/types/conditions.type';

export abstract class GroupBySelectService {
    public static Build(group?: { by: string[]; having: IConditionPrimitives[] }): string {
        if (!group) return '';
        let sqlGroup = ` GROUP BY`;
        group.by.forEach((column, index) => {
            sqlGroup += index === 0 ? ` ${column}` : `, ${column}`;
        });
        sqlGroup += this.assignaments(group.having);
        return sqlGroup;
    }

    private static assignaments(condition?: IConditionPrimitives[]): string {
        if (condition?.length !== 0 && condition) return `${ConditionsService.Build('HAVING', condition)}`;
        return '';
    }
}
