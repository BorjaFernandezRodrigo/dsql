import { IPrefixPrimitive } from '../../../shared/domain/sql/types/prefix.type';

import { IFromPrimitives } from '../types/from.type';

export abstract class FromSelectService {
    public static Build(from?: IFromPrimitives, prefix?: IPrefixPrimitive): string {
        if (!from) return '';
        return ` FROM ${this.tableBuild(from, prefix)}`;
    }

    private static tableBuild(from: IFromPrimitives, prefix?: IPrefixPrimitive): string {
        const formMap: string[] = Object.keys(from).map((key) => {
            const table = prefix ? `${prefix}_${key}` : key;
            const alias = from[key].alias ? `AS ${from[key].alias}` : '';
            return `${table} ${alias}`;
        });
        return formMap.toString();
    }
}
