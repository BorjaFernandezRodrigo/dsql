import { IConditionPrimitives } from '../../../shared/domain/sql/types/conditions.type';

export interface IGroupByPrimitives {
    by: string[];
    having: IConditionPrimitives[];
}
