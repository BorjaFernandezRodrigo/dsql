export interface IColumnsPrimitives {
    [name: string]: {
        alias?: string;
        type?: 'int' | 'string' | 'json' | 'boolean' | 'date' | 'moment' | 'array';
    };
}
