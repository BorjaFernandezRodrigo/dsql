import { IConditionPrimitives } from '../../../shared/domain/sql/types/conditions.type';

import { IColumnsPrimitives } from './columns.type';
import { IFromPrimitives } from './from.type';
import { IGroupByPrimitives } from './groupby.type';
import { IJoinPrimitives } from './joins.type';
import { ILimitPrimitives } from './limit.type';
import { IOrderByPrimitives } from './orderby.type';

export interface ISelectPrimitives {
    columns: '*' | IColumnsPrimitives;
    from?: IFromPrimitives;
    joins?: IJoinPrimitives[];
    where?: IConditionPrimitives[];
    group?: IGroupByPrimitives;
    limit?: ILimitPrimitives;
    orderBy?: IOrderByPrimitives;
}
