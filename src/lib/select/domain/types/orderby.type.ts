export interface IOrderByPrimitives {
    orde: 'ASC' | 'DESC';
    columns: string[];
}
