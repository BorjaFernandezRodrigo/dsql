import { IFromPrimitives } from './from.type';

export interface IJoinPrimitives {
    clausule: '<' | '>' | '<>' | '><';
    from: IFromPrimitives;
    columnsUnion: { [column: string]: string | number };
}
