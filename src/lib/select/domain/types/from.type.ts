export interface IFromPrimitives {
    [name: string]: {
        alias?: string;
    };
}
