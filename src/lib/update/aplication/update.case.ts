import { DsLogger } from '@desenrola/dslogger';
import { Observable, catchError, map, throwError } from 'rxjs';

import { ExecRepositoryError } from '../../shared/domain/error/exec.repository.error';
import { LoggerService } from '../../shared/domain/logger/loggerService';
import { IPipeHandler } from '../../shared/domain/pipe/types/pipe.handler.type';
import { IDriver } from '../../shared/domain/repository/driver.type';
import { IPrefixPrimitive } from '../../shared/domain/sql/types/prefix.type';
import { UpdateSqlBuilderService } from '../domain/services/sql.builder.service';
import { IUpdateResult } from '../domain/types/result.type';
import { IUpdatePrimitives } from '../domain/types/update.type';
import { UpdateAgregator } from '../domain/update.agregator';

export class UpdateCase {
    private repository: IDriver;

    private loggerService: LoggerService;

    constructor(repository: IDriver, logger?: DsLogger) {
        this.repository = repository;
        this.loggerService = new LoggerService(logger);
    }

    public run(request: IUpdatePrimitives, prefix?: IPrefixPrimitive, pipes: IPipeHandler[] = []): Observable<IUpdateResult> {
        const startTime = Date.now();
        const agregator = this.buildAgregator(request);
        const sql = UpdateSqlBuilderService.build(agregator, prefix);
        this.loggerService.addInfoLog(request, sql, prefix).printLog('debug', 'Se ejecuta Update');
        return this.repository.query(sql).pipe(
            catchError((error) => {
                this.loggerService.addError(error).printLog('error', 'Error en Update');
                return throwError(() => new ExecRepositoryError(error));
            }),
            map((result) => {
                const executionTime = Date.now() - startTime;
                this.loggerService.addResult(result).addTime(`${executionTime}ms`).printLog('info', 'Fin de Update');
                return agregator.runPipeline(result, pipes);
            })
        );
    }

    private buildAgregator(request: IUpdatePrimitives): UpdateAgregator {
        return new UpdateAgregator(request.table, request.assignments, request.where);
    }
}
