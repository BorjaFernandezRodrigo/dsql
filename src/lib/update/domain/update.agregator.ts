import { PipelineError } from '../../shared/domain/error/pipeline.error';
import { QueryFormatError } from '../../shared/domain/error/query.format.error';
import { IPipeHandler } from '../../shared/domain/pipe/types/pipe.handler.type';
import { Agregator } from '../../shared/domain/primitives/aggregator';
import { IAssignamentsPrimitives } from '../../shared/domain/sql/types/assignments.type';
import { IConditionPrimitives } from '../../shared/domain/sql/types/conditions.type';
import { ITablePrimitives } from '../../shared/domain/sql/types/table.type';
import { AssignamentsValueObject } from '../../shared/domain/sql/value-object/assignments.vo';
import { TableValueObject } from '../../shared/domain/sql/value-object/table.vo';
import { WhereValueObject } from '../../shared/domain/sql/value-object/where.vo';

import { IResultDriver, IUpdateResult } from '../../public.api';
import { IUpdatePrimitives } from './types/update.type';
import { UpdatePipeline } from './update.pipeline';

export class UpdateAgregator implements Agregator<IUpdatePrimitives> {
    readonly table: TableValueObject;

    readonly assignments: AssignamentsValueObject;

    readonly where?: WhereValueObject;

    private pipeLine: UpdatePipeline;

    constructor(table: ITablePrimitives, assignments: IAssignamentsPrimitives, where?: IConditionPrimitives[]) {
        this.pipeLine = new UpdatePipeline();
        try {
            this.table = new TableValueObject(table);
            this.assignments = new AssignamentsValueObject(assignments);
            this.where = where ? new WhereValueObject(where) : undefined;
        } catch (err) {
            throw new QueryFormatError(err);
        }
    }

    public runPipeline(result: IResultDriver, pipes: IPipeHandler[] = []): IUpdateResult {
        try {
            return this.pipeLine.addPipes(pipes).runPipeLine(result, this.toPrimitives());
        } catch (error) {
            throw new PipelineError(error);
        }
    }

    public toPrimitives(): IUpdatePrimitives {
        return {
            table: this.table.value,
            assignments: this.assignments.value,
            where: this.where?.value,
        };
    }
}
