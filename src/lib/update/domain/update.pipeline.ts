import { PipeLineHandler } from '../../shared/domain/pipe/pipeline.handler';
import { IPipeLineHandler } from '../../shared/domain/pipe/types/pipeline.handler.type';

import { UpdatePipe } from './pipes/update.pipe';

export class UpdatePipeline extends PipeLineHandler implements IPipeLineHandler {
    constructor() {
        super('update');
        super.addPipes([new UpdatePipe()]);
    }
}
