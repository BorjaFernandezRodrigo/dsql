import { IOperation } from '../../../shared/domain/pipe/types/operations.type';
import { IPipeHandler } from '../../../shared/domain/pipe/types/pipe.handler.type';
import { IResultDriver } from '../../../shared/domain/repository/result-driver.type';

import { IUpdateResult } from '../types/result.type';

export class UpdatePipe implements IPipeHandler {
    public runPipe(result: IResultDriver): IUpdateResult {
        return {
            affectedRows: result.affectedRows,
        };
    }

    public canChannel(operation: IOperation): boolean {
        return operation === 'update';
    }
}
