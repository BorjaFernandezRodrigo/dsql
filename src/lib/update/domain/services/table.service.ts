import { IPrefixPrimitive } from '../../../shared/domain/sql/types/prefix.type';
import { ITablePrimitives } from '../../../shared/domain/sql/types/table.type';

export abstract class UpdateTableService {
    public static Build(table: ITablePrimitives, prefix?: IPrefixPrimitive): string {
        return prefix ? `UPDATE ${prefix}_${table}` : `UPDATE ${table}`;
    }
}
