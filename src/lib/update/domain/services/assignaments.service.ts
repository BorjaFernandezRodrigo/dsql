import { IAssignamentsPrimitives } from '../../../shared/domain/sql/types/assignments.type';

export abstract class UpdateAssignamentsService {
    public static Build(assignaments: IAssignamentsPrimitives): string {
        let sqlAssignaments: string = ` SET`;
        Object.keys(assignaments).forEach((column, index) => {
            sqlAssignaments += index === 0 ? ` ${column} = '${assignaments[column]}'` : `, ${column} = '${assignaments[column]}'`;
        });
        return sqlAssignaments;
    }
}
