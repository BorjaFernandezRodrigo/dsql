import { ConditionsService } from '../../../shared/domain/sql/services/conditions.service';
import { IPrefixPrimitive } from '../../../shared/domain/sql/types/prefix.type';

import { UpdateAgregator } from '../update.agregator';
import { UpdateAssignamentsService } from './assignaments.service';
import { UpdateTableService } from './table.service';

export abstract class UpdateSqlBuilderService {
    public static build(entity: UpdateAgregator, prefix?: IPrefixPrimitive): string {
        const sqlTable = UpdateTableService.Build(entity.table.value, prefix);
        const SqlAssignaments = UpdateAssignamentsService.Build(entity.assignments.value);
        const SqlWhere = ConditionsService.Build('WHERE', entity.where?.value);
        return `${sqlTable}${SqlAssignaments}${SqlWhere};`;
    }
}
