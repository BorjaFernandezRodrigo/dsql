export interface IUpdateResult {
    affectedRows: number;
}
