import { DsLogger } from '@desenrola/dslogger';
import { Observable } from 'rxjs';

import { IDeleteResult } from './delete/domain/types/result.type';
import { IInsertResult } from './insert/domain/types/result.type';
import { ISchemeResult } from './scheme/domain/types/result.type';
import { IDSql } from './shared/domain/interface/dsql.type';
import { IPipeHandler } from './shared/domain/pipe/types/pipe.handler.type';
import { IDriver } from './shared/domain/repository/driver.type';
import { IPrefixPrimitive } from './shared/domain/sql/types/prefix.type';
import { IUpdateResult } from './update/domain/types/result.type';

import { DeleteCase } from './delete/aplication/delete.case';
import { InsertCase } from './insert/aplication/insert.case';
import { SchemeCase } from './scheme/aplication/scheme.case';
import { SelectCase } from './select/aplication/select.case';
import { UpdateCase } from './update/aplication/update.case';

import { IDeletePrimitives, IInsertPrimitives, ISelectPrimitives, ITablePrimitives, IUpdatePrimitives } from './public.api';

export class DSql implements IDSql {
    private readonly prefix?: IPrefixPrimitive;

    private readonly driver: IDriver;

    private logger?: DsLogger;

    constructor(driver: IDriver, prefix?: IPrefixPrimitive) {
        this.prefix = prefix;
        this.driver = driver;
    }

    public addLogger(logger: DsLogger) {
        this.logger = logger;
    }

    public select<T>(sql: ISelectPrimitives, pipeLine: IPipeHandler[] = []): Observable<T[]> {
        return new SelectCase<T>(this.driver, this.logger).run(sql, this.prefix, pipeLine);
    }

    public delete(sql: IDeletePrimitives, pipeLine: IPipeHandler[] = []): Observable<IDeleteResult> {
        return new DeleteCase(this.driver, this.logger).run(sql, this.prefix, pipeLine);
    }

    public insert(sql: IInsertPrimitives, pipeLine: IPipeHandler[] = []): Observable<IInsertResult> {
        return new InsertCase(this.driver, this.logger).run(sql, this.prefix, pipeLine);
    }

    public update(sql: IUpdatePrimitives, pipeLine: IPipeHandler[] = []): Observable<IUpdateResult> {
        return new UpdateCase(this.driver, this.logger).run(sql, this.prefix, pipeLine);
    }

    public scheme(table: ITablePrimitives, pipeLine: IPipeHandler[] = []): Observable<ISchemeResult[]> {
        return new SchemeCase(this.driver, this.logger).run(table, this.prefix, pipeLine);
    }
}
