export interface IConditionPrimitives {
    column: string;
    value: string | number | string[] | number[];
    condition: '=' | '!=' | '>' | '>=' | '<' | '<=' | 'in' | 'notIn' | 'between';
}

export type PrefixCondition = 'HAVING' | 'WHERE';
