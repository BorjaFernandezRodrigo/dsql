export interface IAssignamentsPrimitives {
    [column: string]: string | number;
}
