import { ITablePrimitives } from '../types/table.type';
import { SqlValueObject } from './sql.vo';

export class TableValueObject extends SqlValueObject<ITablePrimitives> {
    constructor(value: ITablePrimitives) {
        super(value);
        this.validFormat(value);
    }

    public validFormat(value: ITablePrimitives) {
        this.regexNotValid.forEach((regex) => {
            if (regex.test(value)) throw Error(`Formato table incorreto. value: ${value}`);
        });
    }
}
