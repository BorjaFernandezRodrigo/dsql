import { IConditionPrimitives } from '../types/conditions.type';
import { SqlValueObject } from './sql.vo';

export class WhereValueObject extends SqlValueObject<IConditionPrimitives[]> {
    private msgError = 'Formato where incorreto. ';

    constructor(values: IConditionPrimitives[]) {
        super(values);
        this.validFormat(values);
    }

    public validFormat(values: IConditionPrimitives[]) {
        values.forEach((row) => {
            this.regexNotValid.forEach((regex) => {
                if (regex.test(row.column)) throw Error(this.msgError + `value: ${row.column}`);
                if (typeof row.value === 'string' && regex.test(row.value)) throw Error(this.msgError + `value: ${row.value}`);
                if (Array.isArray(row.value) && row.value.some((value) => regex.test(value.toString())))
                    throw Error(this.msgError + `value: ${row.value}`);
            });
        });
    }
}
