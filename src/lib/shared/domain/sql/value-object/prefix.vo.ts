import { IPrefixPrimitive } from '../../../../public.api';
import { SqlValueObject } from './sql.vo';

export class PrefixValueObject extends SqlValueObject<IPrefixPrimitive> {
    constructor(value: string = '') {
        super(value);
        this.validFormat(value);
    }

    public validFormat(value: IPrefixPrimitive) {
        this.regexNotValid.forEach((regex) => {
            if (regex.test(value)) throw Error(`Formato prefijo incorreto. value: ${value}`);
        });
    }
}
