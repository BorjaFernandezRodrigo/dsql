import { IAssignamentsPrimitives } from '../types/assignments.type';
import { SqlValueObject } from './sql.vo';

export class AssignamentsValueObject extends SqlValueObject<IAssignamentsPrimitives> {
    constructor(value: IAssignamentsPrimitives) {
        super(value);
        this.validFormat(value);
    }

    public validFormat(value: IAssignamentsPrimitives) {
        Object.keys(value).forEach((row) => {
            this.regexNotValid.forEach((regex) => {
                if (regex.test(row) || regex.test(value[row].toString()))
                    throw Error(`Formato assignaments incorreto. value: ${JSON.stringify(value)}`);
            });
        });
    }
}
