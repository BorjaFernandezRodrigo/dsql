import { IColumnsPrimitives } from '../../../../select/domain/types/columns.type';
import { IFromPrimitives } from '../../../../select/domain/types/from.type';
import { IGroupByPrimitives } from '../../../../select/domain/types/groupby.type';
import { IJoinPrimitives } from '../../../../select/domain/types/joins.type';
import { ILimitPrimitives } from '../../../../select/domain/types/limit.type';
import { IOrderByPrimitives } from '../../../../select/domain/types/orderby.type';

import { IAssignamentsPrimitives } from '../types/assignments.type';
import { IConditionPrimitives } from '../types/conditions.type';
import { ITablePrimitives } from '../types/table.type';

// eslint-disable-next-line @typescript-eslint/ban-types
export type Primitives =
    | IColumnsPrimitives
    | IJoinPrimitives
    | IFromPrimitives
    | IConditionPrimitives[]
    | ILimitPrimitives
    | IOrderByPrimitives
    | IGroupByPrimitives
    | ITablePrimitives
    | IAssignamentsPrimitives;

export abstract class SqlValueObject<T extends Primitives> {
    readonly value: T;

    readonly regexNotValid = [
        /[^a-z0-9 _.]+/gim,
        // eslint-disable-next-line max-len
        /(\s*\b(SELECT|UPDATE|DELETE|INSERT|DROP|TRUNCATE|ALTER|GRANT|REVOKE|CREATE|TABLE|VIEW|INDEX|PROCEDURE|FUNCTION|TRIGGER|CONSTRAINT|PRIMARY|FOREIGN|KEY|UNIQUE|CHECK|DEFAULT|NULL|NOT|AND|OR|IN|LIKE|ESCAPE|BETWEEN|IS|NULL|ISNULL|EXISTS|CASE|WHEN|THEN|ELSE|END|IF|FOR|WHILE|DO|REPEAT|UNTIL|LOOP|EXIT|LEAVE|CONTINUE|BREAK|RETURN|GOTO|SET|DECLARE|BEGIN|COMMIT|ROLLBACK|SAVEPOINT|START|TRANSACTION|ISOLATION|LEVEL|READ|WRITE|COMMITTED|UNCOMMITTED|REPEATABLE|SERIALIZABLE|AUTOCOMMIT|NOAUTOCOMMIT|ACCESS|MODE|EXCLUSIVE|SHARE|ROW|SHARE|LOCK|TABLE|LOCK|UNLOCK|NOWAIT|SKIP|LOCKED|FOR|UPDATE|OF|ONLY|READ|WRITE|NEXT|VALUE|FOR|EACH|ROW|LIMIT|OFFSET|ORDER|BY|ASC|DESC|GROUP|BY|HAVING|FROM|WHERE|JOIN|ON|USING|INNER|JOIN|OUTER|JOIN|LEFT|JOIN|RIGHT|JOIN|FULL|JOIN|CROSS|JOIN|NATURAL|JOIN|STRAIGHT_JOIN|SET|GROUP|CONCAT|GROUPING|FUNC|BIT_AND|BIT_OR|BIT_XOR|NOT|IN|SOUNDS|LIKE|REGEXP|RLIKE|BINARY|COLLATE|CHARSET|CHARACTER|SET|AS)\b)/gim,
        /^.{255,}$/,
    ];

    constructor(value: T) {
        this.value = value;
    }

    public abstract validFormat(value: T): void;
}
