import { IConditionPrimitives, PrefixCondition } from '../types/conditions.type';

export abstract class ConditionsService {
    public static Build(prefix: PrefixCondition, conditions?: IConditionPrimitives[]): string {
        let sqlConditions = '';
        if (!conditions || conditions.length === 0) {
            return sqlConditions;
        }
        conditions?.forEach((condition, index) => {
            if ((condition.condition === 'notIn' || condition.condition === 'in') && typeof condition.value === 'object') {
                const operators = condition.value.map((value) => `'${value}'`);
                const sqlCondition = `${condition.column} ${condition.condition === 'in' ? 'IN' : 'NOT IN'} (${operators})`;
                sqlConditions += index === 0 ? `${sqlCondition}` : ` AND ${sqlCondition}`;
            } else if (condition.condition === 'between' && typeof condition.value === 'object') {
                const sqlCondition = `${condition.column} BETWEEN '${condition.value[0]}' AND '${condition.value[1]}'`;
                sqlConditions += index === 0 ? `${sqlCondition}` : ` AND ${sqlCondition}`;
            } else {
                const sqlCondition = `${condition.column} ${condition.condition} '${condition.value}'`;
                sqlConditions += index === 0 ? `${sqlCondition}` : ` AND ${sqlCondition}`;
            }
        });
        return ` ${prefix} ${sqlConditions}`;
    }
}
