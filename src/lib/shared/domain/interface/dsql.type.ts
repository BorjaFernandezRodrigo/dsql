import { Observable } from 'rxjs';

import { IDeletePrimitives } from '../../../delete/domain/types/delete.type';
import { IDeleteResult } from '../../../delete/domain/types/result.type';
import { IInsertPrimitives } from '../../../insert/domain/types/insert.type';
import { IInsertResult } from '../../../insert/domain/types/result.type';
import { ISelectPrimitives } from '../../../select/domain/types/select.type';
import { IUpdateResult } from '../../../update/domain/types/result.type';
import { IUpdatePrimitives } from '../../../update/domain/types/update.type';

import { ISchemeResult, ITablePrimitives } from '../../../public.api';
import { IPipeHandler } from '../pipe/types/pipe.handler.type';

export interface IDSql {
    update: (sql: IUpdatePrimitives, pipeLine?: IPipeHandler[]) => Observable<IUpdateResult>;
    insert: (sql: IInsertPrimitives, pipeLine?: IPipeHandler[]) => Observable<IInsertResult>;
    select: <T>(sql: ISelectPrimitives, pipeLine?: IPipeHandler[]) => Observable<T[]>;
    delete: (sql: IDeletePrimitives, pipeLine?: IPipeHandler[]) => Observable<IDeleteResult>;
    scheme: (table: ITablePrimitives, pipeLine: IPipeHandler[]) => Observable<ISchemeResult[]>;
}
