export interface IConfigDriverPrimitives {
    database: string;
    user?: string;
    password?: string;
    host?: string;
    port?: number;
    timeout?: number;
}
