import { Observable } from 'rxjs';

import { IResultDriver } from './result-driver.type';
import { ISchemeDriver } from './scheme-driver.type';

export interface IDriver {
    query: (sql: string) => Observable<IResultDriver>;
    schemTable: (table: string) => Observable<ISchemeDriver[]>;
}
