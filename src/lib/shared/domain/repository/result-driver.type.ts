export interface IResultDriver {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: { [key: string]: string }[] | [];
    affectedRows: number;
}
