import { Observable } from 'rxjs';

export interface IRepository<IResultDriver> {
    exec(sql: string): Observable<IResultDriver>;
}
