export interface ISchemeDriver {
    column: string;
    type: string;
    nullable: boolean;
    primary: boolean;
    defaulValue?: string;
}
