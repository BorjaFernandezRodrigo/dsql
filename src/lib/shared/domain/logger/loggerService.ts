import { DsLogger, ITagPrimitive } from '@desenrola/dslogger';

export class LoggerService {
    private readonly logger?: DsLogger;

    private log: ITagPrimitive[];

    constructor(logger?: DsLogger) {
        this.logger = logger;
        this.log = [];
    }

    public printLog(levelLog: 'debug' | 'error' | 'info', msg: string) {
        this.logger?.[levelLog](msg, this.log);
    }

    public addInfoLog(request: unknown, sql: string, prefix?: string): this {
        this.log.concat([
            {
                name: 'request',
                value: JSON.stringify(request),
            },
            {
                name: 'sql',
                value: sql,
            },
            {
                name: 'prefix',
                value: prefix ?? '',
            },
        ]);
        return this;
    }

    public addResultPipeline(result: unknown, pipelines: unknown): this {
        this.log.push({
            name: 'result_pipeline',
            value: JSON.stringify(result),
        });
        this.log.push({
            name: 'pipes',
            value: JSON.stringify(pipelines),
        });
        return this;
    }

    public addResult(result: unknown): this {
        this.log.push({
            name: 'result',
            value: JSON.stringify(result),
        });
        return this;
    }

    public addTime(duracion: string): this {
        this.log.push({
            name: 'duracion',
            value: duracion,
        });
        return this;
    }

    public addError(error: string): this {
        this.log.push({
            name: 'error',
            value: JSON.stringify(error),
        });
        return this;
    }
}
