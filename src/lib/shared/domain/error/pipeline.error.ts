export class PipelineError extends Error {
    constructor(msg: string) {
        super(`Error al ejecutar la pipeline. ${msg}`);
    }
}
