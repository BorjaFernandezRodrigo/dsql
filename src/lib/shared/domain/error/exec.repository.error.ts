export class ExecRepositoryError extends Error {
    constructor(msg: string) {
        super(`Error al ejecutar la query en la base de datos. ${msg}`);
    }
}
