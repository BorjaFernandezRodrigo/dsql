export class QueryFormatError extends Error {
    constructor(msg: string) {
        super(`Error al formatear la query. ${msg}`);
    }
}
