/* eslint-disable @typescript-eslint/no-explicit-any */
import { ISqlPrimitives } from '../../primitives/aggregator';
import { IOperation } from './operations.type';

export interface IPipeHandler {
    runPipe: (result: any, sql: ISqlPrimitives) => any;
    canChannel: (operation: IOperation, result: any, sql: ISqlPrimitives) => boolean;
}
