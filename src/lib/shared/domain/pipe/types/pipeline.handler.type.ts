/* eslint-disable @typescript-eslint/no-explicit-any */
import { ISqlPrimitives } from '../../primitives/aggregator';
import { PipeLineHandler } from '../pipeline.handler';
import { IPipeHandler } from './pipe.handler.type';

export interface IPipeLineHandler {
    addPipes: (criteria: IPipeHandler[]) => PipeLineHandler;
    runPipeLine: (result: any, sql: ISqlPrimitives) => any;
}
