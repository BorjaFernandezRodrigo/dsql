/* eslint-disable @typescript-eslint/no-explicit-any */
import { ISqlPrimitives } from '../primitives/aggregator';
import { IOperation } from './types/operations.type';
import { IPipeHandler } from './types/pipe.handler.type';
import { IPipeLineHandler } from './types/pipeline.handler.type';

export class PipeLineHandler implements IPipeLineHandler {
    private pipeLine: IPipeHandler[] = [];

    private readonly operation: IOperation;

    constructor(opepration: IOperation) {
        this.pipeLine = [];
        this.operation = opepration;
    }

    public addPipes(pipeLineHandler: IPipeHandler[]): PipeLineHandler {
        this.pipeLine = this.pipeLine.concat(pipeLineHandler);
        return this;
    }

    public runPipeLine(result: any, sql: ISqlPrimitives): any {
        this.pipeLine.forEach((pipe) => {
            if (pipe.canChannel(this.operation, result, sql)) result = pipe.runPipe(result, sql);
        });
        return result;
    }
}
