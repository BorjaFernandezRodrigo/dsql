import { IDeletePrimitives, IInsertPrimitives, ISelectPrimitives, IUpdatePrimitives } from '../../../public.api';
import { IPipeHandler } from '../pipe/types/pipe.handler.type';

export type ISqlPrimitives = ISelectPrimitives | IDeletePrimitives | IInsertPrimitives | IUpdatePrimitives;

export interface Agregator<T extends ISqlPrimitives> {
    toPrimitives: () => T;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    runPipeline: (result: any | any[], pipes?: IPipeHandler[]) => any;
}
