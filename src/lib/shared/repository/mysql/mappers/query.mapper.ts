import { OkPacket } from 'mysql';

import { IResultDriver } from '../../../../public.api';

export abstract class QueryMysqlMapper {
    public static map(result: { [key: string]: string }[] | OkPacket): IResultDriver {
        return {
            affectedRows: Array.isArray(result) ? result.length : result.affectedRows,
            data: Array.isArray(result) ? result : [],
        };
    }
}
