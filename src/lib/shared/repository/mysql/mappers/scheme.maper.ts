import { ISchemeDriver } from '../../../domain/repository/scheme-driver.type';

export abstract class SchemeMySqlMapper {
    public static map(result: { [key: string]: string }[]): ISchemeDriver[] {
        return result.map((row) => {
            return {
                column: row['COLUMN_NAME'],
                type: row['DATA_TYPE'],
                nullable: row['IS_NULLABLE'] === 'YES',
                primary: row['COLUMN_KEY'] === 'YES',
                defaulValue: row['COLUMN_DEFAULT'] === null ? undefined : row['COLUMN_DEFAULT'],
            };
        });
    }
}
