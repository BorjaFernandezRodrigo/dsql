import mysql, { Connection, MysqlError, OkPacket } from 'mysql';
import { Observable } from 'rxjs';

import { ISchemeDriver } from '../../domain/repository/scheme-driver.type';

import { IConfigDriverPrimitives, IDriver, IResultDriver } from '../../../public.api';
import { QueryMysqlMapper } from './mappers/query.mapper';
import { SchemeMySqlMapper } from './mappers/scheme.maper';

export class MySqlDriver implements IDriver {
    private conector: Connection;

    private config: IConfigDriverPrimitives;

    constructor(config: IConfigDriverPrimitives) {
        this.conector = mysql.createConnection(config);
        this.config = config;
    }

    public query(sql: string): Observable<IResultDriver> {
        return new Observable((observer) => {
            this.conector.query(sql, (error: MysqlError, result: { [key: string]: string }[] | OkPacket) => {
                if (error) {
                    observer.error(error);
                    this.conector.end();
                } else {
                    observer.next(QueryMysqlMapper.map(result));
                    this.conector.end();
                }
                observer.complete();
            });
        });
    }

    public schemTable(tableName: string): Observable<ISchemeDriver[]> {
        const sql = this.buildSchemaQuery(tableName);
        return new Observable((observer) => {
            this.conector.query(sql, (error: MysqlError, result: { [key: string]: string }[]) => {
                if (error) {
                    observer.error(error);
                } else {
                    observer.next(SchemeMySqlMapper.map(result));
                }
                observer.complete();
            });
            this.conector.end();
        });
    }

    private buildSchemaQuery(tableName: string): string {
        const query = `SELECT TABLE_NAME, 
                COLUMN_NAME, 
                COLUMN_DEFAULT, 
                DATA_TYPE, 
                IS_NULLABLE,
                EXTRA,
                COLUMN_KEY
           FROM information_schema.COLUMNS 
          WHERE table_schema = ? 
            AND table_name = ?`;
        return mysql.format(query, [this.config.database, tableName]);
    }
}
