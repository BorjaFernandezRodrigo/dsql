import { Observable, forkJoin, map } from 'rxjs';
import { Database } from 'sqlite3';

import { ISchemeDriver } from '../../domain/repository/scheme-driver.type';

import { IConfigDriverPrimitives, IResultDriver } from '../../../public.api';
import { QuerySqliteMapper } from './mappers/query.mapper';
import { SchemeSqliteMapper } from './mappers/scheme.maper';

// refactoriza esta clase

export class SqlLiteDriver {
    private config: IConfigDriverPrimitives;

    private conector: Database | undefined;

    constructor(config: IConfigDriverPrimitives) {
        this.config = config;
        this.conector = undefined;
    }

    // refactoriza esta funcion
    public query(sql: string): Observable<IResultDriver> {
        return forkJoin({
            connection: this.connect(),
            query: this.execSqlite(sql),
            changes: this.execSqlite('select changes() affectedRows'),
        }).pipe(map((result) => QuerySqliteMapper.map(result)));
    }

    public schemTable(tableName: string): Observable<ISchemeDriver[]> {
        return forkJoin({
            connection: this.connect(),
            query: this.execSqlite(`PRAGMA table_info ("${tableName}");`),
        }).pipe(map((result) => SchemeSqliteMapper.map(result)));
    }

    private execSqlite(sql: string): Observable<{ [key: string]: string }[]> {
        return new Observable((observer) => {
            this.conector?.all(sql, (error: Error | null, rows: { [key: string]: string }[]) => {
                if (error) {
                    observer.error(error);
                } else {
                    observer.next(rows);
                }
                observer.complete();
            });
        });
    }

    private connect(): Observable<boolean> {
        return new Observable((observer) => {
            this.conector = new Database(this.config.database, (error) => {
                if (error) {
                    observer.error(error);
                } else {
                    observer.next(true);
                }
                observer.complete();
            });
        });
    }
}
