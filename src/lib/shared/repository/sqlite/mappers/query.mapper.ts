import { IResultDriver } from '../../../../public.api';

export abstract class QuerySqliteMapper {
    public static map(result: {
        connection: boolean;
        query: {
            [key: string]: string;
        }[];
        changes: {
            [key: string]: string;
        }[];
    }): IResultDriver {
        return {
            affectedRows: Array.isArray(result.query) ? result.query.length : Number(result?.changes[0]['affectedRows']),
            data: result.query || [],
        };
    }
}
