import { ISchemeDriver } from '../../../domain/repository/scheme-driver.type';

export abstract class SchemeSqliteMapper {
    public static map(result: {
        connection: boolean;
        query: {
            [key: string]: string;
        }[];
    }): ISchemeDriver[] {
        return result.query.map((row) => {
            return {
                column: row['name'],
                type: row['type'],
                nullable: row['notnull'] === '1',
                primary: row['pk'] === '1',
                defaulValue: row['dflt_value'] === 'NULL' ? undefined : row['dflt_value'],
            };
        });
    }
}
