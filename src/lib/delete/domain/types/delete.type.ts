import { ITablePrimitives } from '../../../shared/domain/sql/types/table.type';

import { IConditionPrimitives } from '../../../public.api';

export interface IDeletePrimitives {
    table: ITablePrimitives;
    where?: IConditionPrimitives[];
}
