export interface IDeleteResult {
    affectedRows: number;
}
