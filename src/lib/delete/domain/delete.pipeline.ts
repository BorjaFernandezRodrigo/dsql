import { PipeLineHandler } from '../../shared/domain/pipe/pipeline.handler';
import { IPipeLineHandler } from '../../shared/domain/pipe/types/pipeline.handler.type';

import { DeletePipe } from './pipes/delete.pipe';

export class DeletePipeline extends PipeLineHandler implements IPipeLineHandler {
    constructor() {
        super('delete');
        super.addPipes([new DeletePipe()]);
    }
}
