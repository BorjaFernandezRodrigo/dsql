import { PipelineError } from '../../shared/domain/error/pipeline.error';
import { QueryFormatError } from '../../shared/domain/error/query.format.error';
import { IPipeHandler } from '../../shared/domain/pipe/types/pipe.handler.type';
import { Agregator } from '../../shared/domain/primitives/aggregator';
import { IConditionPrimitives } from '../../shared/domain/sql/types/conditions.type';
import { ITablePrimitives } from '../../shared/domain/sql/types/table.type';
import { TableValueObject } from '../../shared/domain/sql/value-object/table.vo';
import { WhereValueObject } from '../../shared/domain/sql/value-object/where.vo';

import { IDeleteResult, IResultDriver } from '../../public.api';
import { DeletePipeline } from './delete.pipeline';
import { IDeletePrimitives } from './types/delete.type';

export class DeleteAgregator implements Agregator<IDeletePrimitives> {
    readonly table: TableValueObject;

    readonly where?: WhereValueObject;

    private pipeLine: DeletePipeline;

    constructor(table: ITablePrimitives, where?: IConditionPrimitives[]) {
        this.pipeLine = new DeletePipeline();
        try {
            this.table = new TableValueObject(table);
            this.where = where ? new WhereValueObject(where) : undefined;
        } catch (err) {
            throw new QueryFormatError(err);
        }
    }

    public runPipeline(result: IResultDriver, pipes: IPipeHandler[] = []): IDeleteResult {
        try {
            return this.pipeLine.addPipes(pipes).runPipeLine(result, this.toPrimitives());
        } catch (error) {
            throw new PipelineError(error);
        }
    }

    public toPrimitives(): IDeletePrimitives {
        return {
            table: this.table.value,
            where: this.where?.value,
        };
    }
}
