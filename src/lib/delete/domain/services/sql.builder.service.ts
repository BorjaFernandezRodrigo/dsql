import { ConditionsService } from '../../../shared/domain/sql/services/conditions.service';
import { IPrefixPrimitive } from '../../../shared/domain/sql/types/prefix.type';

import { DeleteAgregator } from '../delete.agregator';
import { DeleteTableService } from './table.service';

export abstract class DeleteSqlBuilderService {
    public static build(agregator: DeleteAgregator, prefix?: IPrefixPrimitive): string {
        const sqlTable = DeleteTableService.Build(agregator.table.value, prefix);
        const sqlWhere = ConditionsService.Build('WHERE', agregator.where?.value);
        return `${sqlTable}${sqlWhere};`;
    }
}
