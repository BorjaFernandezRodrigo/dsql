import { IPrefixPrimitive } from '../../../shared/domain/sql/types/prefix.type';
import { ITablePrimitives } from '../../../shared/domain/sql/types/table.type';

export abstract class DeleteTableService {
    public static Build(table: ITablePrimitives, prefix?: IPrefixPrimitive): string {
        const sql = prefix ? `${prefix}_${table}` : table;
        return `DELETE FROM ${sql}`;
    }
}
