import { DsLogger } from '@desenrola/dslogger';
import { Observable, catchError, map, throwError } from 'rxjs';

import { ExecRepositoryError } from '../../shared/domain/error/exec.repository.error';
import { LoggerService } from '../../shared/domain/logger/loggerService';
import { IPipeHandler } from '../../shared/domain/pipe/types/pipe.handler.type';
import { IDriver } from '../../shared/domain/repository/driver.type';
import { IPrefixPrimitive } from '../../shared/domain/sql/types/prefix.type';
import { DeleteAgregator } from '../domain/delete.agregator';
import { DeleteSqlBuilderService } from '../domain/services/sql.builder.service';
import { IDeletePrimitives } from '../domain/types/delete.type';
import { IDeleteResult } from '../domain/types/result.type';

export class DeleteCase {
    private repository: IDriver;

    private loggerService: LoggerService;

    constructor(repository: IDriver, logger?: DsLogger) {
        this.repository = repository;
        this.loggerService = new LoggerService(logger);
    }

    public run(request: IDeletePrimitives, prefix?: IPrefixPrimitive, pipes: IPipeHandler[] = []): Observable<IDeleteResult> {
        const startTime = Date.now();
        const agregator = this.buildAgregator(request);
        const sql = DeleteSqlBuilderService.build(agregator, prefix);
        this.loggerService.addInfoLog(request, sql, prefix).printLog('debug', 'Se ejecuta Delete');
        return this.repository.query(sql).pipe(
            catchError((error) => {
                this.loggerService.addError(error).printLog('error', 'Error en Delete');
                return throwError(() => new ExecRepositoryError(error));
            }),
            map((result) => {
                const executionTime = Date.now() - startTime;
                this.loggerService.addResult(result).addTime(`${executionTime}ms`).printLog('info', 'Fin de Delete');
                return agregator.runPipeline(result, pipes);
            })
        );
    }

    private buildAgregator(request: IDeletePrimitives): DeleteAgregator {
        return new DeleteAgregator(request.table, request.where);
    }
}
