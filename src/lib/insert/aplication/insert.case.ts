import { DsLogger } from '@desenrola/dslogger';
import { Observable, catchError, map, throwError } from 'rxjs';

import { ExecRepositoryError } from '../../shared/domain/error/exec.repository.error';
import { LoggerService } from '../../shared/domain/logger/loggerService';
import { IPipeHandler } from '../../shared/domain/pipe/types/pipe.handler.type';
import { IDriver } from '../../shared/domain/repository/driver.type';
import { IPrefixPrimitive } from '../../shared/domain/sql/types/prefix.type';
import { InsertAggregator } from '../domain/insert.agregator';
import { InsertSqlBuilderService } from '../domain/services/sql.builder.service';
import { IInsertPrimitives } from '../domain/types/insert.type';
import { IInsertResult } from '../domain/types/result.type';

export class InsertCase {
    private repository: IDriver;

    private loggerService: LoggerService;

    constructor(repository: IDriver, logger?: DsLogger) {
        this.repository = repository;
        this.loggerService = new LoggerService(logger);
    }

    public run(request: IInsertPrimitives, prefix?: IPrefixPrimitive, pipes: IPipeHandler[] = []): Observable<IInsertResult> {
        const startTime = Date.now();
        const agregator = this.buildAgregator(request);
        const sql = InsertSqlBuilderService.build(agregator, prefix);
        this.loggerService.addInfoLog(request, sql, prefix).printLog('debug', 'Se ejecuta Insert');
        return this.repository.query(sql).pipe(
            catchError((error) => {
                this.loggerService.addError(error).printLog('error', 'Error en Insert');
                return throwError(() => new ExecRepositoryError(error));
            }),
            map((result) => {
                const executionTime = Date.now() - startTime;
                this.loggerService.addResult(result).addTime(`${executionTime}ms`).printLog('info', 'Fin de Insert');
                return agregator.runPipeline(result, pipes);
            })
        );
    }

    private buildAgregator(request: IInsertPrimitives): InsertAggregator {
        return new InsertAggregator(request.table, request.assignments);
    }
}
