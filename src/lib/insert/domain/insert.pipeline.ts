import { PipeLineHandler } from '../../shared/domain/pipe/pipeline.handler';
import { IPipeLineHandler } from '../../shared/domain/pipe/types/pipeline.handler.type';

import { InsertPipe } from './pipes/insert.pipe';

export class InsertPipeline extends PipeLineHandler implements IPipeLineHandler {
    constructor() {
        super('insert');
        super.addPipes([new InsertPipe()]);
    }
}
