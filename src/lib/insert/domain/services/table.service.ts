import { IPrefixPrimitive } from '../../../shared/domain/sql/types/prefix.type';
import { ITablePrimitives } from '../../../shared/domain/sql/types/table.type';

export abstract class InsertTableService {
    public static Build(table: ITablePrimitives, prefix?: IPrefixPrimitive): string {
        return prefix ? `INSERT INTO ${prefix}_${table}` : `INSERT INTO ${table}`;
    }
}
