import { IAssignamentsPrimitives } from '../../../shared/domain/sql/types/assignments.type';

export abstract class InsertAssignamentsService {
    public static Build(assignaments: IAssignamentsPrimitives): string {
        let sqlValues: string = ``;
        let sqlCols: string = ``;
        Object.keys(assignaments).forEach((column, index) => {
            sqlCols += index === 0 ? `${column}` : `, ${column}`;
            sqlValues += index === 0 ? `${assignaments[column]}` : `, ${assignaments[column]}`;
        });
        return ` (${sqlCols}) VALUES (${sqlValues})`;
    }
}
