import { IPrefixPrimitive } from '../../../shared/domain/sql/types/prefix.type';

import { InsertAggregator } from '../insert.agregator';
import { InsertAssignamentsService } from './assignaments.service';
import { InsertTableService } from './table.service';

export abstract class InsertSqlBuilderService {
    public static build(agregator: InsertAggregator, prefix?: IPrefixPrimitive): string {
        const sqlTable = InsertTableService.Build(agregator.table.value, prefix);
        const SqlAssignaments = InsertAssignamentsService.Build(agregator.assignments.value);
        return `${sqlTable}${SqlAssignaments};`;
    }
}
