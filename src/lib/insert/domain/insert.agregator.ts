import { PipelineError } from '../../shared/domain/error/pipeline.error';
import { QueryFormatError } from '../../shared/domain/error/query.format.error';
import { IPipeHandler } from '../../shared/domain/pipe/types/pipe.handler.type';
import { Agregator } from '../../shared/domain/primitives/aggregator';
import { IAssignamentsPrimitives } from '../../shared/domain/sql/types/assignments.type';
import { ITablePrimitives } from '../../shared/domain/sql/types/table.type';
import { AssignamentsValueObject } from '../../shared/domain/sql/value-object/assignments.vo';
import { TableValueObject } from '../../shared/domain/sql/value-object/table.vo';

import { IInsertResult, IResultDriver } from '../../public.api';
import { InsertPipeline } from './insert.pipeline';
import { IInsertPrimitives } from './types/insert.type';

export class InsertAggregator implements Agregator<IInsertPrimitives> {
    readonly table: TableValueObject;

    readonly assignments: AssignamentsValueObject;

    private pipeLine: InsertPipeline;

    constructor(table: ITablePrimitives, assignments: IAssignamentsPrimitives) {
        this.pipeLine = new InsertPipeline();
        try {
            this.table = new TableValueObject(table);
            this.assignments = new AssignamentsValueObject(assignments);
        } catch (err) {
            throw new QueryFormatError(err);
        }
    }

    public runPipeline(result: IResultDriver, pipes: IPipeHandler[] = []): IInsertResult {
        try {
            return this.pipeLine.addPipes(pipes).runPipeLine(result, this.toPrimitives());
        } catch (error) {
            throw new PipelineError(error);
        }
    }

    public toPrimitives(): IInsertPrimitives {
        return {
            table: this.table.value,
            assignments: this.assignments?.value,
        };
    }
}
