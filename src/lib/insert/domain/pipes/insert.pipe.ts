import { IOperation } from '../../../shared/domain/pipe/types/operations.type';
import { IPipeHandler } from '../../../shared/domain/pipe/types/pipe.handler.type';
import { IResultDriver } from '../../../shared/domain/repository/result-driver.type';

import { IInsertResult } from '../types/result.type';

export class InsertPipe implements IPipeHandler {
    public runPipe(result: IResultDriver): IInsertResult {
        return {
            affectedRows: result.affectedRows,
        };
    }

    public canChannel(operation: IOperation): boolean {
        return operation === 'insert';
    }
}
