export interface IInsertResult {
    affectedRows: number;
}
