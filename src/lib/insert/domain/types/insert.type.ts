import { IAssignamentsPrimitives } from '../../../shared/domain/sql/types/assignments.type';
import { ITablePrimitives } from '../../../shared/domain/sql/types/table.type';

export interface IInsertPrimitives {
    table: ITablePrimitives;
    assignments: IAssignamentsPrimitives;
}
