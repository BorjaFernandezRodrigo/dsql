import { PipelineError } from '../../shared/domain/error/pipeline.error';
import { QueryFormatError } from '../../shared/domain/error/query.format.error';
import { Agregator } from '../../shared/domain/primitives/aggregator';
import { IResultDriver } from '../../shared/domain/repository/result-driver.type';
import { ISchemeDriver } from '../../shared/domain/repository/scheme-driver.type';
import { IPrefixPrimitive } from '../../shared/domain/sql/types/prefix.type';
import { PrefixValueObject } from '../../shared/domain/sql/value-object/prefix.vo';
import { TableValueObject } from '../../shared/domain/sql/value-object/table.vo';

import { IPipeHandler } from '../../public.api';
import { SchemePipeline } from './scheme.pipeline';
import { ISchemePrimitives } from './types/scheme.type';

export class SchemeAgregator implements Agregator<ISchemePrimitives> {
    readonly table: TableValueObject;

    readonly prefix?: PrefixValueObject;

    private pipeLine: SchemePipeline;

    constructor(table: string, prefix?: IPrefixPrimitive) {
        this.pipeLine = new SchemePipeline();
        try {
            this.table = new TableValueObject(table);
            this.prefix = prefix ? new PrefixValueObject(prefix) : undefined;
        } catch (err) {
            throw new QueryFormatError(err);
        }
    }

    public runPipeline<R>(result: IResultDriver | ISchemeDriver[], pipes: IPipeHandler[] = []): R[] {
        try {
            return this.pipeLine.addPipes(pipes).runPipeLine(result, this.toPrimitives());
        } catch (error) {
            throw new PipelineError(error);
        }
    }

    public toPrimitives(): ISchemePrimitives {
        return {
            table: this.table.value,
            prefix: this.prefix?.value,
        };
    }
}
