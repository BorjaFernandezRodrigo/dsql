export interface ISchemeResult {
    colum: string;
    defaultValue: string;
    type: string;
    isNullable: boolean;
}
