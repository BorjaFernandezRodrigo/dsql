import { IPrefixPrimitive, ITablePrimitives } from '../../../public.api';

export interface ISchemePrimitives {
    table: ITablePrimitives;
    prefix?: IPrefixPrimitive;
}
