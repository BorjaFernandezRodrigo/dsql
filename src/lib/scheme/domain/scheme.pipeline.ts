import { PipeLineHandler } from '../../shared/domain/pipe/pipeline.handler';
import { IPipeLineHandler } from '../../shared/domain/pipe/types/pipeline.handler.type';

import { SchemePipe } from './pipes/scheme.pipe';

export class SchemePipeline extends PipeLineHandler implements IPipeLineHandler {
    constructor() {
        super('scheme');
        this.addPipes([new SchemePipe()]);
    }
}
