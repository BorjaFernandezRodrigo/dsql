export abstract class SchemeSqlBuilderService {
    public static build(table: string, prefix?: string): string {
        return `${prefix ? prefix + '_' : ''}${table}`.trim();
    }
}
