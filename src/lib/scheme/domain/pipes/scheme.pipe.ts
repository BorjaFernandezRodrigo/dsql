import { IOperation } from '../../../shared/domain/pipe/types/operations.type';
import { IPipeHandler } from '../../../shared/domain/pipe/types/pipe.handler.type';
import { IResultDriver } from '../../../shared/domain/repository/result-driver.type';
import { ISchemeDriver } from '../../../shared/domain/repository/scheme-driver.type';

export class SchemePipe implements IPipeHandler {
    public runPipe(result: IResultDriver | ISchemeDriver[]) {
        if ('data' in result) return (result as IResultDriver).data.map((row) => JSON.parse(JSON.stringify(row)));
        return (result as ISchemeDriver[]).map((row) => JSON.parse(JSON.stringify(row)));
    }

    public canChannel(operation: IOperation): boolean {
        return operation === 'scheme';
    }
}
