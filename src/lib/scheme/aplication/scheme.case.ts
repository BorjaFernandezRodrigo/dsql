import { DsLogger } from '@desenrola/dslogger';
import { Observable, catchError, map, throwError } from 'rxjs';

import { ExecRepositoryError } from '../../shared/domain/error/exec.repository.error';
import { LoggerService } from '../../shared/domain/logger/loggerService';
import { IPipeHandler } from '../../shared/domain/pipe/types/pipe.handler.type';
import { IDriver } from '../../shared/domain/repository/driver.type';
import { IPrefixPrimitive } from '../../shared/domain/sql/types/prefix.type';
import { ITablePrimitives } from '../../shared/domain/sql/types/table.type';
import { SchemeAgregator } from '../domain/scheme.agregator';
import { SchemeSqlBuilderService } from '../domain/services/sql.builder.service';
import { ISchemeResult } from '../domain/types/result.type';

export class SchemeCase {
    private repository: IDriver;

    private loggerService: LoggerService;

    constructor(repository: IDriver, logger?: DsLogger) {
        this.repository = repository;
        this.loggerService = new LoggerService(logger);
    }

    public run(request: ITablePrimitives, prefix?: IPrefixPrimitive, pipes: IPipeHandler[] = []): Observable<ISchemeResult[]> {
        const startTime = Date.now();
        const agregator = this.buildAgregator(request, prefix);
        const sql = SchemeSqlBuilderService.build(agregator.table.value, agregator.prefix?.value);
        this.loggerService.addInfoLog(request, sql, prefix).printLog('debug', 'Se ejecuta Scheme');
        return this.repository.schemTable(sql).pipe(
            catchError((error) => {
                this.loggerService.addError(error).printLog('error', 'Error en Scheme');
                return throwError(() => new ExecRepositoryError(error));
            }),
            map((result) => {
                const executionTime = Date.now() - startTime;
                this.loggerService.addResult(result).addTime(`${executionTime}ms`).printLog('info', 'Fin de Scheme');
                return agregator.runPipeline(result, pipes);
            })
        );
    }

    private buildAgregator(request: ITablePrimitives, prefix?: IPrefixPrimitive) {
        return new SchemeAgregator(request, prefix);
    }
}
