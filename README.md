# DSql

[![NPM](https://img.shields.io/badge/npm-CB3837?style=for-the-badge&logo=npm&logoColor=white)](https://www.npmjs.com/package/@desenrola/dsql)
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/BorjaFernandezRodrigo/dsql)

[![Documentación](https://img.shields.io/badge/view-Documentation-blue)](https://dsql-borjafernandezrodrigo-5deef242ae47432500e1b23e22f22d3.gitlab.io/)
[![pipeline status](https://gitlab.com/BorjaFernandezRodrigo/dsql/badges/main/pipeline.svg)](https://gitlab.com/BorjaFernandezRodrigo/dsql/-/commits/main)
[![coverage report](https://gitlab.com/BorjaFernandezRodrigo/dsql/badges/main/coverage.svg)](https://gitlab.com/BorjaFernandezRodrigo/dsql/-/commits/main)
[![Latest Release](https://gitlab.com/BorjaFernandezRodrigo/dsql/-/badges/release.svg)](https://gitlab.com/BorjaFernandezRodrigo/dsql/-/releases)

> **Una librería para la conexión y ejecución de sentencias DML en bases de datos sql.**

## Motivación

Dsql es una abstracción para la conexión de bases de datos. Presta una interfaz
estandarizada y amigable para la interacción con el lenguaje DML.

La librería permita la inyección del driver a utilizar (clase que funciona como
conector final con la BD), además de permitir inyectar pipelines para la
transformación del resultado, lo que permite una versatilidad mayor para la
personalización del funcionamiento.

## Uso

### Instalación

```bash
  [sudo] npm install @desenrola/dsql
```

```bash
  curl https://npmjs.org/install.sh | sh
```

### Inicializando Driver

```ts
    import { MySqlDriver, SqlLiteDriver } from '@desenrola/dsql/public.api';

    const mysqlDriver = new MySqlDriver({
        host: '127.0.0.1',
        user: 'admin',
        password: 'abc123..',
        database: 'dslq_tests',
    });


    const driver = new SqlLiteDriver(
            database: join(`${__dirname}/tools/dslq_tests.sqlite`),
    });
```

### Realizando una select básica

```ts
    import { MySqlDriver, DSql } from '@desenrola/dsql/public.api';

    const mysqlDriver = new MySqlDriver({
        host: '127.0.0.1',
        user: 'admin',
        password: 'abc123..',
        database: 'dslq_tests',
    });

    const instance = new DSql(driver, 'prefix');
    instance.select<Type>({
        columns: '*',
        from: { table: {} },
    }).subscribe({
        next: (result: Type[]) => {
            ...
        },
        error: (err) => {
            ...
        },
    });
```

### Realizando una select con tipado

```ts
    import { ISelectPrimitives } from '@desenrola/dsql/public.api';

    const sql: ISelectPrimitives = {
        columns: {
            col1: {
                alias: 'Col1',
                type: 'string',
            },
            col2: {
                alias: 'Col2',
                type: 'int',
            },
        },
        from: { select_tests1: {} },
    };

    instance.select<Type>(sql).subscribe({
        next: (result: Type[]) => {
            ...
        },
        error: (err) => {
            ...
        },
    });
```

### Realizando una select compleja

```ts
    import { ISelectPrimitives } from '@desenrola/dsql/public.api';

    const sql: ISelectPrimitives = {
        columns: {
            'table1.char_value': {
                alias: 'Col1',
                type: 'string',
            },
            'table1.int_value': {
                alias: 'Col2',
                type: 'int',
            },
            'table1.datetime_value': {
                alias: 'Col3',
                type: 'moment',
            },
            'table1.date_value': {
                alias: 'Col4',
                type: 'moment',
            },
        },
        from: { 'select_tests1': { alias: 'table1'} },
        joins: [
            {
                clausule: '><',
                columnsUnion: {
                    'table1.char_value': 'table2.char_value',
                },
                from: { 'select_tests2':  { alias: 'table2'} },
            },
            {
                clausule: '><',
                columnsUnion: {
                    'table1.char_value': 'table3.char_value',
                },
                from: { select_tests3: { alias: 'table3' } },
            },
        ],
        where: [
            {
                column: 'table1.int_value',
                value: [1, 2],
                condition: 'between',
            },
            {
                column: 'table1.char_value',
                condition: 'in',
                value: ['text'],
            },
            {
                column: 'table1.int_value',
                condition: '=',
                value: 1,
            },
        ],
        group: {
            by: ['Col1', 'Col2', 'COl3', 'COl4'],
            having: [
                {
                    column: 'Col1',
                    condition: '=',
                    value: 'text',
                },
            ],
        },
        limit: 1,
    };

    instance.select<Type>(sql).subscribe({
        next: (result: Type[]) => {
            ...
        },
        error: (err) => {
            ...
        },
    });
```

## Inyectar Pipe

Los pipes nos permiten hac3er tranformaciones sobre el resultado que nos devuelve
el repositorio. Todas las operaciones contra el repositorio son subseptibles de
inyecctar una pipe o un conkjusnto de estas.

Su extructura responde al patron cadena de responsabilidad, lo que permite
inyectar pipes que dependiendo el contexto se ejecuten o no.

```ts
export class TestPipe implements IPipeHandler {
    public runPipe() {
        return {
            col1: 'test 1',
            col2: 'test 2',
        };
    }

    public canChannel(operation: IOperation): boolean {
        return operation === 'select';
    }
}
```

```ts
import { DSql, MySqlDriver } from '@desenrola/dsql/public.api';

const mysqlDriver = new MySqlDriver({
    host: '127.0.0.1',
    user: 'admin',
    password: 'abc123..',
    database: 'dslq_tests',
});

const pipe = new TestPipe();
const instance = new DSql(driver, 'prefix');
this.instance.select(sql, [pipe]);

// Resultado
// {
//     col1: 'test 1',
//     col2: 'test 2',
// };
```

## Inyectar Logger

La libreria Dsql, tiene integracion con la libreria [Dslogger](https://www.npmjs.com/package/@desenrola/dslogger),
por lo que se puede añadir un log para poder informacion de la ejecucion.

Lo niveles que se utilizan son

-   Debug: se imprime con cada eecucion antes de la ejecucion
-   Info: indica el tiempo de ejecucion
-   Error: indicar los errores de los drivers

```ts
import { ApiPublisher, ConsolePublisher, DsLogger, FilePublisher, LocalStoragePublisher } from '@desenrola/ds-loger';
import { DSql, MySqlDriver } from '@desenrola/dsql/public.api';

const mysqlDriver = new MySqlDriver({
    host: '127.0.0.1',
    user: 'admin',
    password: 'abc123..',
    database: 'dslq_tests',
});

const filePublisher = new FilePublisher('error', location);

const logger = new DsLogger().addPublisher(filePublisher);
const pipe = new TestPipe();
const instance = new DSql(driver, 'prefix', logger);
this.instance.select(sql, [pipe]);
```

## Ejecutar pruebas

Todas las pruebas de DSql están escritas en jest y diseñadas para ejecutarse con
npm.

```bash
    npm test
```

## Desarrolladores

Autor: [Borja Fernández Rodrigo](https://gitlab.com/BorjaFernandezRodrigo)

Contribuidores: [Desenrola](https://gitlab.com/desenrolapp)
