# Registro de cambios

Todos los cambios de la libreira, se integraran en este docuemnto.

---

## Nota

Los tipos que se tendran encuenta en este registro son:

-   `Added` funcionalidades nuevas.
-   `Changed` cambios en las funcionalidades existentes.
-   `Deprecated` indicar que una característica o funcionalidad está obsoleta y
    que se eliminará en las próximas versiones.
-   `Removed` características en desuso que se eliminaron en esta versión.
-   `Fixed` corrección de errores.
-   `Security` vulnerabilidades.

## Nota

Notacion de versionado semantico utilizado:

-   x -> version de la aplicacion, se producen cambion sustaciales
-   z -> versión minor, se añaden nuevas funcionalidades
-   j -> versión patch coreccion de errores

---

## Indice

-   [1.0.0](#release-100)

## Release 1.0.0

-   Se añade drivers para mysql|mariadb y sqlite
-   Se añade posibilidad de injectar pipes, para modificar el resultado
-   Se añade soporte para [DsLogger](https://www.npmjs.com/package/@desenrola/dslogger)
    (solo level debug, info y error)
-   Se añade consulta scheme para tablas
